package p04_extensions_reified.s03_inline

object Metrics {
    fun collect(name: String, duration: Long) {
        //do something
    }
}

inline fun <T> timed(body: () -> T): T {
    val start = System.currentTimeMillis()
    try {
        return body()
    } finally {
        val duration = System.currentTimeMillis() - start
        println(duration)
    }
}

inline fun <T> timedWithMetric(metricsName: String? = null, body: () -> T): T {
    val start = System.currentTimeMillis()
    try {
        return body()
    } finally {
        val duration = System.currentTimeMillis() - start
        println(duration)
        if (metricsName != null) {
            Metrics.collect(metricsName, duration)
        }
    }
}

fun add(a: Int, b: Int) = timedWithMetric {
    a + b
}

fun main(args: Array<String>) {
    add(1, 2)
}