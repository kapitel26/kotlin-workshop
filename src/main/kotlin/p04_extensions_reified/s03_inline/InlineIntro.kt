@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s03_inline.inlineintro

fun repeatNotInline(times: Int, action: (Int) -> Unit) {
    for (index in 0 until times) {
        action(index)
    }
}

inline fun repeatInline(times: Int, action: (Int) -> Unit) {
    for (index in 0 until times) {
        action(index)
    }
}

fun main(args: Array<String>) {
    repeatNotInline(5) {
        println("NotInline")
    }

    repeatInline(5) {
        println("Inline")
    }
}

