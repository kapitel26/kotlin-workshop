@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.w01_standard_lambdas.solution

import org.h2.jdbcx.JdbcDataSource


fun main(args: Array<String>) {
    level1()
    level2(args)
}

fun level1() {
    val datasource = JdbcDataSource().apply {
        setUrl("jdbc:h2:mem:singleton")
        user = "sa"
    }

    datasource.connection.use { connection ->
        connection.createStatement().use { statement ->
            statement.executeUpdate(
                """
                CREATE TABLE PERSON(
                    id int auto_increment primary key,
                    firstname varchar(100),
                    lastname varchar(100))
                """.trimIndent()
            )
        }
    }
}

fun level2(args: Array<String>) {
    val (p1, p2, p3) = args.takeIf { it.size in listOf(3, 4) }
            ?: throw IllegalStateException("3 or 4 parameter required")

    val p4 = args.takeIf { it.size == 4 }?.let {
        it[4]
    }
}
