@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.w01_standard_lambdas.excercise

import org.h2.jdbcx.JdbcDataSource

/*
Nutzen Sie die Standard-Funktionen von Kotlin, um den folgenden Code idomatischer/kürzer zu schreiben
 */
fun main(args: Array<String>) {
    level1()
    level2(args)
}

fun level1() {
    val datasource = JdbcDataSource()
    datasource.setURL("jdbc:h2:mem:singleton")
    datasource.user = "sa"

    val connection = datasource.connection
    try {
        val statement = connection.createStatement()
        try {
            statement.executeUpdate(
                """
                CREATE TABLE PERSON(
                    id int auto_increment primary key,
                    firstname varchar(100),
                    lastname varchar(100))
                """.trimIndent()
            )
        } finally {
            statement.close()
        }
    } finally {
        connection.close()
    }
}

fun level2(args: Array<String>) {
    if (args.size != 3 || args.size != 4) {
        throw IllegalStateException("3 or 4 parameter required")
    }

    val (p1, p2, p3) = args

    val p4 = if (args.size == 4) {
        args[4]
    } else {
        null
    }
}



