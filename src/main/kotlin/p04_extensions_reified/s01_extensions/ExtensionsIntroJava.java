package p04_extensions_reified.s01_extensions;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

public class ExtensionsIntroJava {
    public static void main(String[] args) throws MalformedURLException {
        URL localhostUrl = StringUtil.toUrl("http://localhost");

        Person rene = new Person("Rene","rp@etosquare.de");
        PersonDTO personDTO = PersonDTOMapper.mapFrom(rene);
    }
}


class StringUtil {
    public static URL toUrl(String url) throws MalformedURLException {
        return new URL(url);
    }
}

class Person {
    private String name;
    private String eMail;

    public Person(String name, String eMail) {
        this.name = name;
        this.eMail = eMail;
    }

    public String getName() {
        return name;
    }

    public String getEMail() {
        return eMail;
    }
}

class PersonDTO implements Serializable {
    private String name;

    public PersonDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class PersonDTOMapper {
    public static PersonDTO mapFrom(Person person) {
        return new PersonDTO(person.getName());
    }
}

