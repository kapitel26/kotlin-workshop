@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s01_extensions.generics

//Notwendig für VM
@kotlin.jvm.JvmName("addAllInt")
fun List<Int>.addAll(): Int =
    fold(0) { i1, i2 -> i1 + i2 }


@kotlin.jvm.JvmName("addAllString")
fun List<String>.addAll(): Int =
    fold(0) { s1, s2 -> s1.toInt() + s2.toInt() }


fun main(args: Array<String>) {
    val allInt = listOf(1,2).addAll()
    val allString  = listOf("1","2").addAll()

    println("Int: $allInt String: $allString")

    //siehe auch average
    val average = listOf(1,2,3).average()
    println(average)
}