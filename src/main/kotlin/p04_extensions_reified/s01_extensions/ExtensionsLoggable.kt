@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s01_extensions.loggable

import kotlin.math.PI

interface Loggable {
    val loggerName: String
        get() = this::class.qualifiedName ?: "unknown"
}

//Nicht direkt im Interface definieren, damit die Funktion nicht überschrieben werden kann.
fun Loggable.info(msg: String) {
    println("$loggerName: $msg")
}

//<editor-fold defaultstate="collapsed" desc="2. Stufe Erweiterbarkeit">

//Das könnte in einem anderen Modul stehen oder nachträglich implementiert werden
fun <T> Loggable.withTracing(prefix: String, block: () -> T): T = try {
    info("$prefix - begin")
    block()
} finally {
    info("$prefix - end")
}

//</editor-fold>

class Circle(val radius: Double) : Loggable {
    init {
        info("Init circle")
    }

    //<editor-fold defaultstate="collapsed" desc="2. Stufe Erweiterbarkeit">
    val area: Double
        get() = withTracing("area") {
            PI * radius * radius
        }
    //</editor-fold>
}

fun main(args: Array<String>) {
    val c = Circle(10.0)
    println(c.area)
}
