@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s01_extensions.intro

import java.net.URL

fun main(args: Array<String>) {
    //Aufruf erfolgt wie bei normaler Methode
    val url1 = "http://localhost".toURL()

    val rene = Person("Rene", "rp@etosquare.de")
    val reneDTO = rene.toDTO()

    println(reneDTO)
}

//Erweiterung der String-Klasse um Funktion toURL()
fun String.toURL() = URL(this)

class Person(val name: String, val email: String)
data class PersonDTO(val name: String)

fun Person.toDTO(): PersonDTO = PersonDTO(name)
