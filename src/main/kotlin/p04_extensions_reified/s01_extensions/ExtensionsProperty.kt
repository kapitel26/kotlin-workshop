package p04_extensions_reified.s01_extensions

import kotlin.math.PI

class Circle(val radius: Double)
class Rectangle(val width: Double, val height: Double)

val Circle.area: Double
    get() = PI * radius * radius

val Rectangle.area: Double
    get() = width * height


fun main(args: Array<String>) {
    val c = Circle(10.0)
    val r = Rectangle(5.0, 4.0)

    println("c: ${c.area} r: ${r.area}")
}