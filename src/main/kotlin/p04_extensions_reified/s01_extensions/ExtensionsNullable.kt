@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s01_extensions.nullable

fun List<Double>?.addAll(): Double {
    if (this == null) {
        return 0.0
    }
    return fold(0.0) { i1, i2 -> i1 + i2 }
}


fun main(args: Array<String>) {
    val nullList: List<Double>? = null
    println(nullList.addAll())

    //siehe auch
    println(nullList.orEmpty())
}