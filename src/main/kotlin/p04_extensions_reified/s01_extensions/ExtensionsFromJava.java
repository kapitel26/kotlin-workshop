package p04_extensions_reified.s01_extensions;


import p04_extensions_reified.s01_extensions.generics.ExtensionsGenericsKt;
import p04_extensions_reified.s01_extensions.intro.ExtensionsIntroKt;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class ExtensionsFromJava {
    public static void main(String[] args) {
        String localhost = "http://localhost";
        URL url = ExtensionsIntroKt.toURL(localhost);
        System.out.println(url);

        List<Integer> ints = Arrays.asList(1, 2);
        int average = ExtensionsGenericsKt.addAllInt(ints);
        System.out.println(average);
    }
}
