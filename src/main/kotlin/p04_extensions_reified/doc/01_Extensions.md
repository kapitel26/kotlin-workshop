# Extensions

* Extensions erweitern vorhandene Klassen/Interfaces um zusätzlichen Methoden und Properties
  (siehe `s01_extensions/ExtensionsIntro`)  
    ```kotlin
    fun String.toURL() = URL(this)
    ```
* Müssen explizit importiert werden 
* Können keine vorhandenen Methoden überschreiben bzw. redefinieren
* Werden intern als statische Methoden implementiert (IDEA: `Tools/Kotlin/Show Kotlin Bytecode + Decompile`)

    ```kotlin
    public final class ExtensionsIntroKt {
       ... 
       @NotNull
       public static final URL toURL(@NotNull String $receiver) {
          Intrinsics.checkParameterIsNotNull($receiver, "receiver$0");
          return new URL($receiver);
       }
    
       @NotNull
       public static final PersonDTO toDTO(@NotNull Person $receiver) {
          Intrinsics.checkParameterIsNotNull($receiver, "receiver$0");
          return new PersonDTO($receiver.getName());
       }
    }
    ```

* In Java können Extensions wie normale statische Funktionen aufgerufen werden (siehe `s01_extensions/ExtensionsFromJava`)
    ```kotlin
    String localhost = "http://localhost";
    URL url = ExtensionsIntroKt.toURL(localhost);
    ```
    
# Extensions-Properties
* Extensions-Properties können wie Methoden hinzugefügt werden (siehe `s01_extensions/ExtensionsProperty` )
    ```kotlin
    class Circle(val radius: Double)
      
    val Circle.area: Double
          get() = 2 * PI * radius              
    ```
* Es können keine neuen Datenfelder hingezufügt werden
 
    
# Einsätze von Extensions
* Als Ersatz für Util-Klassen
* Als Ersatz für Vererbung
* Zur Modularisierung / Erweiterbarkeit von Libraries/Framework (siehe `s01_extensions/ExtensionsLoggable`)    
    ```kotlin
    interface Loggable {
        val loggerName: String
            get() = this::class.qualifiedName ?: "unknown"
    }
    
    //Nicht direkt im Interface definieren, damit die Funktion nicht überschrieben werden kann.
    fun Loggable.info(msg: String) {
        println("$loggerName: $msg")
    }
    ```
    
    ```kotlin
    //Das könnte in einem anderen Modul stehen oder nachträglich implementiert werden
    fun <T> Loggable.withTracing(prefix: String, block: () -> T): T = try {
        info("$prefix - begin")
        block()
    } finally {
        info("$prefix - end")
    }
    ```

