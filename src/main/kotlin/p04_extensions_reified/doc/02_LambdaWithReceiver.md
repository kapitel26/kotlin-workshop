# Lambda With Receiver 


* Eine kleine Wiederholung zu  _normalen_ Lambdas   
    siehe  `s02_lambda_with_receiver/LambdaReceiverIntro`
    ```kotlin
    repeat(5) { index ->
        println(index)
    }
    ```
* _Normale_ Lambdas werden wie folgt als Funktions-Parameter definiert und aufgerufen:
    ```kotlin
    fun repeat(times: Int, action: (Int) -> Unit) {
        for (index in 0 until times) {
            action(index)
        }
    }     
    ```
    
* _LambdaWithReceiver_ werden wie anonyme Extensions-Funktionen definiert: `<Type>.(...) -> <Result>`, 
d.h. sie bekommen zusätzlich noch einen Typ vorangestellt:
    siehe  `s02_lambda_with_receiver/LambdaReceiverIntro`
    ```kotlin
    fun with(receiver: Person, block: Person.() -> Unit)
    ```     
* Das Aufrufen einer Funktion mit einem solchen Lambda ist wie immer, 
nur das innerhalb des Blockes der `this`-Zeiger den angegeben Typ hat:
    ```kotlin
    val rene = Person("Rene", "Preissel","rp@etosquare.de")
    with(rene) {
        ///this Zeiger ist eine Person
        println("$firstName $surname $email")
    }
    ``` 
* Beim Aufruf des Lambdas muss der `Receiver`, der zukünftige `this`-Zeiger explizit angegeben werden:
    ```
    fun with(receiver: Person, block: Person.() -> Unit) {
        receiver.block() //Alternativ: block(receiver)
    }
    ```    
    
# Standard-Lambda-Scope-Funktionen in Kotlin

## `with`, `run`, `let`
siehe `s02_lambda_with_receiver/LambdaReceiverStandard`
* `with` gibt es auch als generische Standard-Funktion mit Return-Wert
    ```kotlin
    val fullnameWith = with(rene) {
        "$firstName $surname"
    }
    ``` 
    
* Wenn der Parameter `null` sein kann, dann ist die Extension-Funktion `run` die bessere Wahl. 
Diese kann mit dem `?.`-Operator kombiniert werden 
Funktioniert ansonsten wie `with`, d.h. der `this`-Zeiger wird im Lambda redefiniert
    ```
    val addressRun: String? = rene.address?.run {
        "$zip $city"
    }
    ```
* Wenn der `this`-Zeiger nicht redefiniert werden soll, sondern `it` oder ein 
konkreter Parameter benutzt werden soll, ist `let` die richtige Wahl.
    ```
    val addressLet: String? = rene.address?.let {
            "${it.zip} ${it.city}"
        }
    ```

##  `apply`, `also`
siehe `s02_lambda_with_receiver/LambdaReceiverStandard`

* Wenn man zwar etwas mit dem Receiver tun möchte, 
aber als Ergebnis das Ausgangsobjekt weiterverwenden will, ist ```apply``` passend.
`apply` redefiniert auch den `this`-Zeiger wie die `with`-Funktion.
`apply` wird typischerweise bei der Initialisiserung verwendet:
    ```kotlin
    val mapApply = HashMap<String,String>().apply {
        put("Mo", "Monday")
        put("Tu", "Tuesday")
    }
    ```
* Das Äquivalent mit expliziten Parameter ist die Funktion `also`:
    ```kotlin
    val mapAlso = HashMap<String,String>().also { map ->
        map.put("Mo", "Monday")
        map.put("Tu", "Tuesday")
    }
    ```
    
## `takeIf`
* `takeIf` ist eine _normale_ Lambda-Funktion und ist als Extension für beliebige Typen definiert.
  `takeIf` erwartet eine Predicate-Funktion, die ihrerseits als Lambda-Parameter das aktuelle Objekt übergeben bekommt.
  Wenn das Predicate `true` zurückliefert, dann wird das aktuelle Objekt zurückgegeben ansonsten `null`. 
  Zusammen mit dem `?.`-Operator kann  man so kompakte `if`-Ketten schreiben.
    
    ```kotlin
    rene.address?.takeIf { it.zip.startsWith("2") }?.let { address-> 
        println(address)
    }
    ```
    
* Es gibt auch die Funktion `takeUnless`, die das Predicate genau entgegengesetzt interpretiert.
    
## `use` 
* `use` ist eine normale Lambda-Funktion die beim Umgang mit `Closable` Ressourcen hilft.
Sie ersetzt das `try(...)` aus Java:

    ```kotlin
    File("address.txt").writer().use {
        it.write(createAddresssLabel(rene))
    }
    ```     
  

## Entscheidungs-Hilfe
![](_standard_functions_decision.png) 

von ['Mastering Kotlin standard functions: run, with, let, also and apply'](https://medium.com/@elye.project/mastering-kotlin-standard-functions-run-with-let-also-and-apply-9cd334b0ef84)
