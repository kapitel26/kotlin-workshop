# Inline Functions - Abstractions without costs

* Bei Inline-Funktionen wird der Funktions-Rumpf an die Aufrufstelle kopiert. Sinnvoll bei eigenen Kontrollstrukturen mit Lambdas:
  (siehe `s03_inline/InlineIntro`)  
    ```kotlin
    fun repeatNotInline(times: Int, action: (Int) -> Unit) {
        for (index in 0 until times) {
            action(index)
        }
    }  
    inline fun repeatInline(times: Int, action: (Int) -> Unit) {
        for (index in 0 until times) {
            action(index)
        }
    }

    repeatNotInline(5) {
        println("NotInline")
    }
    repeatInline(5) {
        println("Inline")
    }
    ``` 
    
* Decompiliert
    ```java
    repeatNotInline(5, (Function1)null.INSTANCE);
    int times$iv = 5;
    int var2 = 0;
    
    for(byte var3 = times$iv; var2 < var3; ++var2) {
       String var5 = "Inline";
       System.out.println(var5);
    }
    ```    
    
# Compiler lässt toten Code einfach weg
    siehe `s03_inline/InlineTimed`