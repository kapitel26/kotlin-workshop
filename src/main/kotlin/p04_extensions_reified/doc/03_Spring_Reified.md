# Spring-Boot-Kotlin-Extensions

* Spring bringt seit der Version 5.0.0 und SpringBoot seit der Version 2.0.0 eigene
  Extensions um den Umgang mit der Spring-API zu vereinfachen  
* Damit Kotlin und Spring gut zusammenarbeiten sollten/müssen alle Beans als `open` definiert werden.
  Alternativ nimmt man ein entsprechendes _Gradle_ oder _Maven_ Plugin.  
  
## Ohne Extensions mit Konstruktor-Injection    
* Erstes Beispiel ohne Extensions (siehe `s04_spring_reified/SprintBootIntroPure`):  
    ```kotlin
    @Service
    class HelloWorldService {
        fun sayHello() = "Hello"
    }
    
    @RestController
    class HelloWorldController(val service: HelloWorldService) {
        @GetMapping("/hello")
        fun hello() = service.sayHello()
    }

    
    @SpringBootApplication
    class WebApplication
    
    fun main(args: Array<String>) {
        SpringApplication.run(WebApplication::class.java, *args)
    }
    ```  

* Konstruktor-Injection ist mit Kotlin sehr kompakt umzusetzen    
    
    
## Inline Reified erspart Übergabe von Class-Parametern    
* Mit _Inline Reified_ kann innerhalb einer generischen `inline`-Funktion auf den Typparameter zugegriffen werden.
  Durch _Inline_ wird das _Type Erasure_ der JVM umgangen.  (siehe `s04_spring_reified/SprintBootReified`): 
    ```kotlin     
    fun main(args: Array<String>) {
        runApplication<WebApplication>(*args)
    }
    ```
    
    Dabei ist `runApplication` wie folgt definiert:
    ```kotlin
    inline fun <reified T : Any> runApplication(vararg args: String): ConfigurableApplicationContext =
        SpringApplication.run(T::class.java, *args)
    ```    
    
* Auch der Zugriff auf Beans im Context ist über `reified` ohne Typeangabe möglich
    ```kotlin
    val service: HelloWorldService = context.getBean()
    ```   
    Alternativ:
    ```kotlin
    val service  = context.getBean<HelloWorldService>()
    ```    
    
## Beans-DSL anstelle von Annotations
* Es gibt seit Spring 5 eine funktionale [Beans-DSL](https://docs.spring.io/spring-framework/docs/5.0.0.RELEASE/spring-framework-reference/kotlin.html#bean-definition-dsl) um Beans zu registrieren. Durch verschiedene Kotlin-Extensions ist diese
noch einfacher zu benutzen: (siehe `s04_spring_reified/SprintBootBeansDSL`)  
    ```kotlin
    class WorldService {
        fun sayWorld() = "World"
    }
    
    class HelloService(val worldService: WorldService) {
        fun sayHello() = "Hello"
        fun sayHelloWorld() = "Hello ${worldService.sayWorld()}"
    }

    val beanDefinitions = beans {
        bean<WorldService>() //bean() ist reified um den Typ über den generischen Parameter zu ermitteln
        bean("helloWorld") {
            HelloService(ref()) //ref() ist inline reified und findet automatisch das richtige Bean
        }
    }

    val context = GenericApplicationContext().apply {
        beanDefinitions.initialize(this) // die Definitions werden auf den Context angewendet
        refresh()
    }
    ```  
    
# Routing DSL
* Es gibt auch eine neue [Routing-DSL](https://docs.spring.io/spring-framework/docs/5.0.0.RELEASE/spring-framework-reference/kotlin.html#webflux-functional-dsl) um das Mapping von HTTP-Aufrufen funktional umzusetzen:
  (siehe `s04_spring_reified/SprintBootRoutingDSL`)    
    ```kotlin
    val beanDefinitions = beans {
        bean<WorldService>()
        bean("helloWorld") {
            HelloService(ref())
        }
        bean {
            //ref() erlaubt Zugriff auf registrierte Beans  
            val helloService = ref<HelloService>()
            router {
                GET("/hello") {
                    ServerResponse.ok().syncBody(helloService.sayHelloWorld())
                }
                GET("/hello/{count}") {
                    val count = it.pathVariable("count").toInt()
                    val result = (1..count).map { helloService.sayHelloWorld() }.joinToString()
                    ServerResponse.ok().syncBody(result)
                }
            }
        }
    }

    runApplication<WebfluxApplicationDSL>(*args) {
        addInitializers(beanDefinitions)
    }
    ```