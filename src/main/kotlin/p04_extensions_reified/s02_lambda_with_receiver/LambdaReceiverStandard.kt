@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s02_lambda_with_receiver.standard

import java.io.File


data class Address(val city: String, val zip: String)
class Person(val firstName: String, val surname: String, val email: String, val address: Address? = null)

fun createAddresssLabel(person: Person): String = with(person) {
    """
    $firstName $surname
    ${address?.zip} ${address?.city}
    """.trimIndent()
}

fun main(args: Array<String>) {
    val rene = Person("Rene", "Preissel", "rp@etosquare.de")

    val fullnameWith = with(rene) {
        "$firstName $surname"
    }

    val addressRun: String? = rene.address?.run {
        "$zip $city"
    }

    val addressLet: String? = rene.address?.let {
        "${it.zip} ${it.city}"
    }

    rene.address?.let { address ->
        println(address)
    }

    val mapApply = HashMap<String,String>().apply {
        put("Mo", "Monday")
        put("Tu", "Tuesday")
    }

    val mapAlso = HashMap<String,String>().also {map ->
        map.put("Mo", "Monday")
        map.put("Tu", "Tuesday")
    }

    rene.address?.takeIf { it.zip.startsWith("2") }?.let { address->
        println(address)
    }

    File("address.txt").writer().use {
        it.write(createAddresssLabel(rene))
    }
}

