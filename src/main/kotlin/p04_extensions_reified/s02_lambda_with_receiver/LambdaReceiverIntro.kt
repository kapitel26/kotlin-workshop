@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s02_lambda_with_receiver.intro


class Person(val firstName: String, val surname: String, val email: String)


fun main(args: Array<String>) {
    repeat(5) {
        println(it)
    }

    repeat(5) {index ->
        println(index)
    }


    val rene = Person("Rene", "Preissel","rp@etosquare.de")

    //Wiederholung der Variable
    println("${rene.firstName} ${rene.surname} ${rene.email} ")

    with(rene) {
        //this Zeiger ist eine Person
        println("$firstName $surname $email")
    }
}

//Beispielhafte Implementierung
fun with(receiver: Person, block: Person.() -> Unit) {
    receiver.block() //ALternativ: block(receiver)
}