@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s02_lambda_with_receiver.loggable

import kotlin.math.PI

interface Loggable {
    val loggerName: String
        get() = this::class.qualifiedName ?: "unknown"
}

fun Loggable.info(msg: String) {
    println("$loggerName: $msg")
}

class TracingContext(val prefix: String, val loggable: Loggable) {
    fun <T> T.dump(name: String) : T = apply { //apply liefert this zeiger als Ergebnis zurück
        loggable.info("$prefix - dump $name: ${this ?: "empty"}")
    }
}

fun <T> Loggable.withTracing(prefix: String, block: TracingContext.() -> T): T = try {
    info("$prefix - begin")
    TracingContext(prefix,this).run(block) // run führt den übergebenen Block mit TracingContext als this-Zeiger aus
} finally {
    info("$prefix - end")
}

class Circle(val radius: Double) : Loggable {
    val area: Double
        get() = withTracing("area") {//TracingContext ist hier der this-Zeiger
            PI * radius * radius.dump("r") //dump kann nur innerhalb dieser Methode benutzt werden
        }
}

fun main(args: Array<String>) {
    val c  = Circle(10.0)
    println(c.area)
}
