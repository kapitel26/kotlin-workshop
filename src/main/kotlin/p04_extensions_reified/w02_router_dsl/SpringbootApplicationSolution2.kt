@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.w02_router_dsl.solution2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.support.beans
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import p04_extensions_reified.w02_router_dsl.FeiertageService
import java.time.LocalDate

val PREFIXE_BUNDESLAENDER = listOf("/bayern" to "Bayern", "/hamburg" to "Hamburg", "" to null)

@SpringBootApplication
class WebApplication {
    @Bean
    fun feiertagService() = FeiertageService()
}


fun main(args: Array<String>) {

    val beanDefinitions = beans {
        bean {
            val feiertageService = ref<FeiertageService>()
            router {
                for (prefix in PREFIXE_BUNDESLAENDER) {
                    GET("${prefix.first}/today") {
                        ServerResponse.ok()
                            .syncBody(
                                feiertageService.feiertage(
                                    LocalDate.now(),
                                    LocalDate.now(),
                                    prefix.second
                                ).isNotEmpty()
                            )
                    }
                    GET("${prefix.first}/{jahr}") {
                        val jahr = it.pathVariable("jahr").toInt()
                        val feiertage = feiertageService.feiertage(
                            LocalDate.of(jahr, 1, 1),
                            LocalDate.of(jahr, 12, 31),
                            prefix.second
                        )
                        ServerResponse.ok()
                            .syncBody(feiertage)
                    }
                }
            }
        }
    }

    runApplication<WebApplication>(*args) {
        addInitializers(beanDefinitions)
    }
}

