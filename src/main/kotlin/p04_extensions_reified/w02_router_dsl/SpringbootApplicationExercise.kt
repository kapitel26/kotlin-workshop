@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.w02_router_dsl.exercise

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import p04_extensions_reified.w02_router_dsl.FeiertageService
import java.time.LocalDate

val PREFIXE_BUNDESLAENDER = listOf("/bayern" to "Bayern", "/hamburg" to "Hamburg", "" to null)

/*
Level 1
Konvertieren Sie die annotation-basierte feiertageFuerJahr()-Funktion zur funktionalen Router API.
Das Routing für `/today` ist bereits am Ende dieser Datei beispielhaft umgesetzt und muss nur einkommentiert werden.

Level 2
Erzeugen Sie dynamisch anhand der PREFIXE_BUNDESLAENDER-Liste weitere Routings, d.h. `/bayern/2018`, `/hamburg/2018` und "/2018"
Für das Ermitteln der Daten bietet die feiertage()-Funktion einen zusätzlichen Parameter. feiertage(von,bis,bundesland)
 */
@RestController
class FeiertagController(val feiertageService: FeiertageService) {

    @GetMapping("/today")
    fun today() = feiertageService.feiertage(LocalDate.now(), LocalDate.now()).isNotEmpty()

    @GetMapping("/{jahr}")
    fun feiertageFuerJahr(@PathVariable("jahr") jahr: Int) =
        feiertageService.feiertage(LocalDate.of(jahr, 1, 1), LocalDate.of(jahr, 12, 31))

}

@SpringBootApplication
class WebApplication {
    @Bean
    fun feiertagService() = FeiertageService()

}

fun main(args: Array<String>) {
    runApplication<WebApplication>(*args)
}

//fun main(args: Array<String>) {
//    val beanDefinitions = beans {
//        bean {
//            val feiertageService = ref<FeiertageService>()
//            router {
//                GET("/today") {
//                    ServerResponse.ok()
//                        .syncBody(feiertageService.feiertage(LocalDate.now(), LocalDate.now()).isNotEmpty())
//                }
//            }
//        }
//    }
//
//    runApplication<WebApplication>(*args) {
//        addInitializers(beanDefinitions)
//    }
//}
