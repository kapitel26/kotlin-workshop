@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s04_spring_reified.introreified


import org.springframework.beans.factory.getBean
import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@Service
class HelloWorldService {
    fun sayHello() = "Hello"
}

@RestController
class HelloWorldController(val service: HelloWorldService) {
    @GetMapping("/hello")
    fun hello() = service.sayHello()
}


@SpringBootApplication
class WebApplication

fun main(args: Array<String>) {
//    runApplication<WebApplication>(*args)

    val context = runApplication<WebApplication>(*args) {
        //this ist hier SpringApplication
        setBannerMode(Banner.Mode.OFF)
    }

    val service: HelloWorldService = context.getBean()
    println(service.sayHello())
}
