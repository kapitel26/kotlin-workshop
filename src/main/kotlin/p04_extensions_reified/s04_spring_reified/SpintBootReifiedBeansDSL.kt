@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s04_spring_reified.beansdsl


import org.springframework.beans.factory.getBean
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans

class WorldService {
    fun sayWorld() = "World"
}

class HelloService(val worldService: WorldService) {
    fun sayHelloWorld() = "Hello ${worldService.sayWorld()}"
}


fun main(args: Array<String>) {

    val beanDefinitions = beans {
        bean<WorldService>() //bean() ist reified um den Typ über den generischen Parameter zu ermitteln
        bean("helloWorld") {
            HelloService(ref()) //ref() ist inline reified und findet automatisch das richtige Bean
        }
    }

    val context = GenericApplicationContext().apply {
        beanDefinitions.initialize(this) // die Definitions werden auf den Context angewendet
        refresh()
    }

    val service: HelloService = context.getBean()
    println(service.sayHelloWorld())

}
