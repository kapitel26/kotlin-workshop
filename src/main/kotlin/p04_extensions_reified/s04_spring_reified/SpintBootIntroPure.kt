@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s04_spring_reified.intropure


import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@Service
class HelloWorldService {
    fun sayHello() = "Hello"
}

@RestController
class HelloWorldController(val service: HelloWorldService) {
    @GetMapping("/hello")
    fun hello() = service.sayHello()
}

@SpringBootApplication
class WebApplication

fun main(args: Array<String>) {
    SpringApplication.run(WebApplication::class.java, *args)
}
