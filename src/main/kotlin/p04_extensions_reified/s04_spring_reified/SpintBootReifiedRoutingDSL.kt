@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_reified.s04_spring_reified.routingdsl


import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.support.beans
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class WorldService {
    fun sayWorld() = "World"
}

class HelloService(val worldService: WorldService) {
    fun sayHelloWorld() = "Hello ${worldService.sayWorld()}"
}

@SpringBootApplication
class WebfluxApplicationDSL

fun main(args: Array<String>) {

    val beanDefinitions = beans {
        bean<WorldService>()
        bean("helloWorld") {
            HelloService(ref())
        }
        bean {
            //ref() erlaubt Zugriff auf registrierte Beans
            val helloService = ref<HelloService>()
            router {
                GET("/hello") {
                    ServerResponse.ok().syncBody(helloService.sayHelloWorld())
                }
                GET("/hello/{count}") {
                    val count = it.pathVariable("count").toInt()
                    val result = (1..count).map { helloService.sayHelloWorld() }.joinToString()
                    ServerResponse.ok().syncBody(result)
                }
            }
        }
    }

    runApplication<WebfluxApplicationDSL>(*args) {
        addInitializers(beanDefinitions)
    }
}
