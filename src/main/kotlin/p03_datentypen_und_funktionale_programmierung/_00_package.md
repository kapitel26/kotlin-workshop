# Package p02_oo_klassen

## Datentypen

Kotlin ist, wie Java, *statisch typisiert*, d. h. wird schon Compilezeit geprüft, ob Funktionen mit den passenden Parametern aufgerufen werden. Anders als in Java darf, dank *Type Inference* an vielen Stellen Typedeklaratonen weglassen, wenn aus dem Kontext klar ist, welcher Typ gilt.

 * Type Inference
 * Elementare Datentypen
 * Generics and Collections
 * `null`
 * enums

## Funktionale Programmierung

 * Lambdas (inklusive `it`, `return`)
 * Methodenreferenzen
 * Streaming API auf Collections