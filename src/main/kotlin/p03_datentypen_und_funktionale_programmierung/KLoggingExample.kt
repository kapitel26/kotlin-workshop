package p03_datentypen_und_funktionale_programmierung

import mu.KLogging
import kotlin.math.PI


// wo companion ist
class Circle(val radius: Double) {
    fun diameter(): Double {
        logger.info("compute diameter")
        return radius * 2
    }

    val diameter: Double
        get() {
            logger.info("compute diameter")
            return radius * 2
        }

    val circumference: Double
        get() {
            logger.info {
                "compute circumference"
            }
            return diameter() * PI
        }

    companion object : KLogging()
}

fun main(args: Array<String>) {
    val circle = Circle(42.0)
    Circle.logger.info("static access")
}