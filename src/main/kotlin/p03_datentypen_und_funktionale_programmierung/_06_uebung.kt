package p03_datentypen_und_funktionale_programmierung

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Percentage.withPercentage
import org.junit.Test

/**
 * ### Übung: Hier werden Äpfel mit Birnen verglichen
 *
 * Es geht darum zu ermitteln, wieviel Umsatz mit Äpfeln und Birnen
 * jeweils gemacht wurde. Die Daten liegen im CSV-Format for.
 *
 *  1. Parsen der CSV-Strings und Umwandlung in Liste von [UmsatzPosition](en)
 *
 *  2. Gruppieren der Daten nach Obstsorte, und summieren der Umsätze
 *
 *  3. Sortieren und Formatieren für die Ausgabe.
 *
 *  4. (Extra Challenge): Umgang mit fehlerhaften Zeilen, die mehr
 *     oder weniger als zwei Zeilen haben.
 *
 */
class AepfelVsBirnen {

    val verkaufsDatenClean = """
            Äpfel;2.11
            Äpfel;0.79
            Birnen;2.49
            Äpfel;3.43
            Birnen;0.99
    """.trimIndent().lines()

    data class UmsatzPosition(val artikel: String, val betrag: Double)

    @Test
    fun `_01 CSV Daten Parsen`() {

        val positionen: List<UmsatzPosition> =
        // AUFGABE
        // Nimm `verkaufsDatenClean`, splitte nach Semikolon,
        // bereinige den Artikel-String, wandle den Betrag in Double
        // und erzeuge Umsatzpositionen.
                emptyList()

        assertThat(positionen)
                .hasSize(5)
                .startsWith(UmsatzPosition("Äpfel", 2.11))
                .endsWith(UmsatzPosition("Birnen", 0.99))
    }


    @Test
    fun `_02 Nach Artikel gruppieren und summieren`() {

        val gruppierteUmsaetze: Map<String, Double> =
        // AUFGABE
        // Gruppiere die Umsätze nach Artikel
        // Extrahiere den Betrag aus jedem Umsatz
        // und summiere die Beträge.
                emptyMap()


        assertThat(gruppierteUmsaetze).hasSize(2)
        assertThat(gruppierteUmsaetze["Äpfel"]).isCloseTo(6.33, withPercentage(0.001))
        assertThat(gruppierteUmsaetze["Birnen"]).isCloseTo(3.48, withPercentage(0.001))
    }

    @Test
    fun `_03 Sortieren und Formatieren`() {

        fun formatUmsatz(artikel: String, summe: Double) =
                "%-12s %.2f €".format(artikel, summe)

        val sortierteAusgabe: List<String> =
                emptyList()

        sortierteAusgabe.forEach { println(it) }

        assertThat(sortierteAusgabe).containsExactly(
                "Birnen       3.48 €",
                "Äpfel        6.33 €"
        )

    }


    val verkaufsDatenDirty = """
            Äpfel;2.11
               Äpfel; 0.79
            Birnen; 2.49
            Birnen; 2.49; nix

            Birnen
            Äpfel;3.43
               Äpfel;1.11
            Birnen;0.99
    """.trimIndent().lines()

    @Test
    fun `04 Cleansing`() {

        fun formatUmsatz(artikel: String, summe: Double) =
                "%-12s %.2f €".format(artikel, summe)

        val (goodRows, badRows) =
                Pair(emptyList<List<String>>(), emptyList<List<String>>())

        val sortierteAusgabe: List<String> =
                emptyList()

        sortierteAusgabe.forEach { println(it) }

        assertThat(sortierteAusgabe).containsExactly(
                "Birnen       3.48 €",
                "Äpfel        7.44 €"
        )

        assertThat(badRows).containsExactly(
                listOf("Birnen", " 2.49", " nix"),
                listOf(""),
                listOf("Birnen")
        )


    }

}
