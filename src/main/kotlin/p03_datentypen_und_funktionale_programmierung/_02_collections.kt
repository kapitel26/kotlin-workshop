package p03_datentypen_und_funktionale_programmierung

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/**
 * ## Collections
 *
 * Natürlich gibt es in Kotlin aus Collections, wie List, Set, Map etc.
 *
 * * In der Regel erzeugt man die Collections per Konstruktur sondern
 *   mit Factory-Funktionen aus `kotlin.collections`, z. B. `listOf()`
 *
 * * Es gibt sowohl veränderbare als auch unveränderbare Collections.
 *   Die Factory-Funktionen und Interface gibt es daher paarweise, z. B.
 *   `mapOf()` und `mutableMapOf()` bzw. `List` und `MutableMap`.
 *
 * * Für den Zugriff auf Listen kann die Array-Notation genutzt werden, z. B. `names[4]`
 *   - Echte Java-Arrays werden kaum direkt verwendet
 *
 * * Für den Zugriff auf Maps kann ebenfalls die Array-Notation genutzt werden, z. B. `phoneNumbers["Müller"]`.
 *
 */
class _02_collections {


    /**
     * ## Listen
     *
     * * werden idR mit `listOf(...)` erzeugt.
     *
     * * implementieren das Interface `kotlin.collections.List` für Read-Only-Listen
     *
     * * können `for`-beschleift werden (`in`-Notation)
     *
     * *
     *
     */
    @Test
    fun `01 Listen`() {

        val liste =

                listOf(23, 42, 4711)

        assertThat(liste is List)

        for (i in liste)
            println("i = $i")

        assertThat(
                liste[0]
        ).isEqualTo(23)

        assertThat(
                liste[1]
        ).isEqualTo(42)

        assertThat(
                liste.last()
        ).isEqualTo(4711)

        assertThat(
                liste.size
        ).isEqualTo(3)
    }

    @Test
    fun `02 Set`() {

        val l =


                setOf(2, 5, 5, 2, 1)


        assertThat(
                l.size
        ).isEqualTo(3)

        assertThat(
                l.sum()
        ).isEqualTo(1 + 2 + 5)
    }


    /**
     * ## Pairs
     *
     *
     */
    @Test
    fun `04 Pair`() {

        var p =


                Pair(2, 4)


        var (a, b) = p                      // Destructuring mal wieder

        assertThat(a).isEqualTo(2)
        assertThat(b).isEqualTo(4)

        p =

                47 to 11                   // Infix Notation


        assertThat(p.first).isEqualTo(47)
        assertThat(p.second).isEqualTo(11)

    }


    @Test
    fun `03 Map`() {

        val map =


                mapOf(
                        "König" to 4,
                        "Dame" to 3,
                        "Bube" to 2
                )


        assertThat(map["Bube"]).isEqualTo(2)
        assertThat(map["Dame"]).isEqualTo(3)
        assertThat(map["König"]).isEqualTo(4)
    }



    @Test
    fun `05 Die "normalen" Collections sind unveränderlich (Immutable)`() {

        val map1 = mapOf("König" to 4)


        // map["Dame"] = 3     // geht nicht, kein schreibender Zugriff


        val map2 = map1.plus("Dame" to 3) // erzeugt neue Map


        assertThat(map1).isNotSameAs(map2)

        assertThat(map1["König"]).isEqualTo(4)
        assertThat(map1["Dame"]).isNull()

        assertThat(map2["König"]).isEqualTo(4)
        assertThat(map2["Dame"]).isEqualTo(3)
    }


    @Test
    fun `06 Veränderbare (mutable) Collections gibt es natürlich auch`() {

        val map =


                mutableMapOf("König" to 4)


        assertThat(map["Dame"]).isNull()


        map["Dame"] = 3


        assertThat(map["König"]).isEqualTo(4)
        assertThat(map["Dame"]).isEqualTo(3)
    }

    @Test
    fun `07 Und die guten Alten von Java kann man auch nehmen, wenn man möchte`() {

        val map =


                java.util.HashMap<String, Int>()


        map["König"] = 4
        map["Dame"] = 3


        assertThat(map["König"]).isEqualTo(4)
        assertThat(map["Dame"]).isEqualTo(3)

    }

    @Test
    fun `08 Folgen von Werten (Ranges) und for-Schleifen`() {

        val range = 1..3

        for (i in range) {
            println(i)
        }

        for (i in 0 until 7) {
            println(i)
        }
        // entspricht for(int i=0; i < 7; i++)

        val l = listOf("COBOL", "Java", "Kotlin").reversed()
        for ((i, v) in l.withIndex()) {
            println(" Nr. $i ist $v")
        }

        for (i in 0 until 7) {
            println(i)
        }

        println((100 downTo 0 step 10).toList())

    }
}
