package p03_datentypen_und_funktionale_programmierung

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.Test

/**
 * ## Kotlin und die `null`-Pointer und die *Smart Casts*
 *
 * Ein wesentlicher Unterschied zu Java ist der Umgang mit `null`.
 * In der Regel darf Variablen oder Paramtern kein `null`-Wert
 * zugewisen werden, außer dies ist explizit deklariert
 * (mit der `?`-Notation).
 * Dadurch erreicht man eine viel höhere Sicherheit vor
 * `NullPointerException`s, denn wo kein null-Pointer hinsoll,
 * da kommt auch keiner hin.
 *
 * Wo es aber doch erforderlich mit Variable zu arbeiten,
 * die als Optional deklariert sind (mit `?`),
 * bietet Kotlin eine Reihe von nützlichen Notationen
 * und Funktionen, die dafür sorgen, dass der Code nicht
 * mit länglichen `null`-Check aufgebläht werden muss.
 */
class _04_null {

    /**
     * ### Datentypen
     *
     * Jeder Datentyp entspricht einer Kotlin-Klasse,
     * und umfasst Instanzen dieser Klasse, nicht
     * aber `null`.
     *
     * Erst durch ein angehängtes `?` wird der Wertebereich
     * um `null` erweitert.
     */
    @Test
    fun `01 Variablen können nicht "genullt" werden`() {

        var s = "ein String"


        // s = null geht nicht


        var s2: String? = "ein String"

        s2 = null


        assertThat(s2 == null).isTrue()
    }

    /**
     * ## Optionale Datentypen mit "`?`"
     *
     * Jedem Datentyp kann ein `?` angehängt werden.
     * Sind ist zusätzlich zum normalen Wertebereich aus `null` zulässig.
     */
    @Test
    fun `02 Optionale Typen mit "?"`() {

        var s2: String? = "ein String"


        s2 = null


        assertThat(s2 == null).isTrue()
        assertThat(s2.isNullOrBlank()).isTrue()
    }

    /**
     * ## null-Pointer aus Java-Aufrufen
     *
     * Methoden aus Java-Bibliothen kann man in der Regel
     * nicht ansehen, ob sie `null` liefern können.
     * Kotlin geht damit wie folgt um.
     *
     * * Bei `?`-Typen wird das null durchgereicht.
     *
     * * `?` wird auch von der Type-Inferenz geliefert.
     *
     * * Bei `null`-freien Typen wird zum Zeitpunkt der Zuweisung eine
     *   Exception durchgereicht.
     */
    @Test
    fun `03 null-Values als Rückgabewerte von Java-Klassen`() {


        println(System.getProperties())
        val p1: String? = System.getProperty("os.name")


        assertThat(p1).isNotEmpty()


        val p2: String? = System.getProperty("gipsnich")


        assertThat(p2).isNull()


        val p3 = System.getProperty("gipsnich")


        assertThat(p3).isNull()
        assertThat(p3 is String?)
                .isTrue()
                .`as`("Die Type Inference geht davon aus, " +
                        "dass null geliefert werden könnte."
                )

        assertThatThrownBy {


            val p4: String = System.getProperty("gipsnich")


            println(p3)
        }
                .hasMessageContaining("must not be null")


    }

    /**
     * ## Der Elvis-Operator
     */
    @Test
    fun `04 Der Elvis-Operator`() {

        listOf("Hamlet", null).forEach {

            val name =
                    it ?: "<Name>"

            println(name)
            assertThat(name).isNotEmpty()
        }
    }


    /**
     * Da das Werfen einer Exception auch eine Ausdruck ist,
     * kann, kann es auch im Elvis-Operator für den Null-Fallgenutzt werden.
     */
    @Test
    fun `05 Exceptional Elvis`() {

        fun frissOderStirb(food: String?) =


                food ?: throw RuntimeException("Das ist das Ende")



        assertThat(frissOderStirb("Apfel")).isEqualTo("Apfel")

        assertThatThrownBy {
            frissOderStirb(null)
        }.hasMessageContaining("Ende")


    }


    /**
     * ## Der `?.`-Operator
     */
    @Test
    fun `06 Optionale felder referenzieren`() {

        data class LinkedList(val value: Any, val tail: LinkedList?)

        val linkedList = LinkedList(1, LinkedList(2, LinkedList(3, null)))

        // linkedList.tail.tail.value)  // nicht erlaubt, wg. null

        assertThat(
                linkedList.tail?.tail?.value
        ).isEqualTo(3)

        assertThat(
                linkedList.tail?.tail?.tail?.tail?.value
        ).isNull()
    }


    /**
     * ## *Smart Casts*
     *
     * Die Type-Inference von Kotlin geht über die Deklaration von Variablen hinaus.
     * Wenn in Fallunterscheidungen (z. B. `if`, `when`) beispielsweise,
     * `x != null` oder `x is String` geprüft wird,
     * dann wird `x` in dem entsprechenden Zweig auch so gecasted.
     *
     *
     */
    @Test
    fun `07 Smart casts bei null Werten`() {

        listOf("hallo", null, "Welt").forEach { s: String? ->

            if (
                    s != null
            ) {
                println(s.toUpperCase())
                assertThat(s is String)
                // println(s?.toUpperCase())  // Hier nicht nötig, weil Kotlin hier weiss,
                // dass s nicht null ist.
            } else {
                // println(s.toUpperCase())   // Hier nicht möglich, weil s ja null ist.
                println("No value")
            }
        }
    }


    @Test
    fun `08 Smart Casts mit beliebigen Typen`() {

        listOf(
                listOf(3, 4, 5),
                setOf(5, 6),
                listOf(7, 8, 9)
        ).forEach { c: Collection<Int> ->

            if (c is List) {
                // Hier ist c: List<Int>
                println("A list starting with ${c[0]}")
            } else {
                // Hier ist c: Collection<Int>
                // println("A list starting with ${c[0]}")
                println("A collection with ${c.size} elements")

            }
        }
    }

}
