package p03_datentypen_und_funktionale_programmierung

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/**
 * ## Generics
 *
 * Kotlin kennt, wie Java, Generic, also generisch parametriebare Datentypen.
 * Diese sind jedoch komfortabler zu verwenden, weil man an vielen
 * Stellen Typangaben weglassen kann (und sollte), wenn
 * sich aus dem Kontext ergibt, welche Typen gemeint sind.
 *
 * In diesem Abschnitt wird die elementare Verwendung demonstriert.
 *
 * Im Umgang mit Vererbung sind die Kotlin-Generic denen von Java überlegen
 * (Stichworte: Kovarianz, Kontravarianz). Dies wird jedoch erst in
 * einem späteren Abschnitt demonstriert.
 *
 */
class _03_generics {

    @Test
    fun `01 explizite Angabe der Typ-Parameter`() {

        val list =


                mutableListOf<Int>()


        list.add(42)

        val javaList =


                ArrayList<Int>()


        list.add(42)
    }

    @Test
    fun `02 Inferenz  der Typ-Parameter`() {
        val list =

                listOf(42, 1234565789012453L)


        assertThat(list is List<Number>)

        val p =

                Pair(11, "Moin")


        assertThat(p is Pair<Int, String>)
    }

    @Test
    fun `03 Eigene generische Klassen`() {


        data class AnnotatedValue<V : Number>
        constructor(val value: V, val description: String)


        val v1 = AnnotatedValue(3.14, "Not really PI")
        assertThat(v1 is AnnotatedValue<Double>)

        val v2 = AnnotatedValue(3, "Even less PI-like")
        assertThat(v2 is AnnotatedValue<Int>)
    }

}
