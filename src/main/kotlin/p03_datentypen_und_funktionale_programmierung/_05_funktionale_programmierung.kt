package p03_datentypen_und_funktionale_programmierung

import io.ktor.util.random
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.math.BigInteger

/**
 * ## Funktionale Programmierung
 *
 * Kotlin ist keine funktionale Programmiersprache sondern eine Hybride Sprache,
 * ähnlich wie Scala. Aber Kotlin bringt genug funktionale Aspekte mit,
 * um in Kotlin-Systemen funktional zu programmieren, wo es angebracht ist.
 *
 * ### Was macht eigentlich funktionale Programmierung aus?
 *
 * 1. *Programmieren in Funktionen* mit Eingabeparametern und Rückgabewerten
 * 2. *Seiteneffektfreiheit*, d. h. bei gleichen Eingabewerten liefert
 *    eine Funktion denselben (Rückgabe-) Wert, analog zu
 *    mathematischen Funktionen.
 * 3. *Higher Order Functions*, d. h. Funktionen, die Funktionen verarbeiten
 *    oder erzeugen.
 *
 */
class _05_funktionale_programmierung {

    /**
     * ### Ein Anwendungsbeispiel für die Verarbeitung eines Datenstroms
     */
    @Test
    fun `01 Ein kleine Demo`() {

        val stichproben = 10000

        fun wuerfeln() = random(6) + 1

        (1..stichproben)
                .map { stichprobe ->
                    Pair(wuerfeln(), wuerfeln())
                }
                .groupBy { (augen1, augen2) ->
                    augen1 + augen2
                }
                .mapValues { (augenzahl, wurfErgebnisse) ->
                    wurfErgebnisse.size
                }
                .mapValues { (augenzahl, anzahlErgebnisse) ->
                    anzahlErgebnisse * 100 / stichproben
                }
                .mapValues { (augenzahl, prozentAnteil) ->
                    "%2d: %s".format(augenzahl, "|".padEnd(prozentAnteil, '*'))
                }
                .toSortedMap()
                .values
                .map(::println)
    }


    /**
     * ## Programmieren in Funktonen
     *
     * Ein paar Merkmale und Features von Kotlin unterstützen
     * funktionale Programmieransätze:
     *
     *  * Jede Funktion hat einen Rückgabewert.
     *
     *  * Das `when` Konstrukt für Fallunterscheidungen.
     *
     *  * Wichtige Kontrollstrukturen sind als Ausdruck einsetzbar,
     *    z. B. `when`, `try`, `if` und `throw`.
     *
     *  * Funktionen können lokal definiert werden.
     *
     *  * Optimierung von Tailrecursion wird unterstützt (`tailrec`).
     */
    @Test
    fun `01 Funktionen deklarieren`() {


        fun fac(n: Int): BigInteger =
                when (n) {
                    1 ->
                        1.toBigInteger()
                    else ->
                        n.toBigInteger() * fac(n - 1)
                }


        assertThat(fac(5)).isEqualTo(120)
        assertThat(fac(42)).isEqualTo(
                BigInteger("1405006117752879898543142606244511569936384000000000")
        )


        tailrec fun fac2(n: BigInteger, p: BigInteger = 1.toBigInteger()): BigInteger =
                when (n) {
                    1.toBigInteger() ->
                        p
                    else ->
                        fac2(n - 1.toBigInteger(), p * n)
                }


        assertThat(fac2(42.toBigInteger())).isEqualTo(
                BigInteger("1405006117752879898543142606244511569936384000000000")
        )


    }


    /**
     * ### Higher Order Functions und Lambdas
     *
     */
    @Test
    fun `02 Lambdas`() {

        val helloLambda =


                { a: String -> "Hallo $a!" }



        assertThat(


                helloLambda("Welt")


        )
                .isEqualTo("Hallo Welt!")
                .`as`("Lambdas können Variablen zugewiesen " +
                        "und wie Funktonen aufgerufen werden."
                )

        assertThat(


                { i: Int -> i + i }(4)


        )
                .isEqualTo(8)
                .`as`("Lambdas könen mit Paramterklammen auch " +
                        "anonym aufgerufen werden."
                )

    }

    /**
     * Natürlich haben auch Lambdas in Kotlin einen Typ,
     * und dieser kann verwendet werden um Variablen oder
     * Parameter deklarieren, die Lambdas halten können.
     */
    @Test
    fun `03 Man kann den Typ eines Lambdas natürlich auch explizit deklarieren`() {

        val betrag:


                (Double, String) -> String


        betrag = { b, w -> "$b $w" }

        assertThat(betrag(47.11, "EUR")).isEqualTo("47.11 EUR")
    }


    /**
     * Mit Lambdas als Parameter kann man *Higher Order Functions* schreiben,
     * d. h. Funktionen, die Funktionen verarbeiten.
     *
     * Für den Aufruf gibt es ein wenig *syntactic Sugar*:
     * Ist der letzte Parameter ein Lambda,
     *
     *     listOf(1,2,3).forEach( { ... } )
     *
     * darf man
     * einen Lambda-Block setzen hinter die Klammer setzen.
     *
     *     listOf(1,2.3).forEach() { ... }
     *
     * Ist das Lambda der einzige Parameter, dar man auch die Klammern weglassen:
     *
     *     listOf(1,2.3).forEach { ... }
     *
     * Damit sehen die Aufrufe *Higher Order Functions* aus wie die von
     * eingebauten Kontrollstrukturen.
     *
     *     when { ... }
     *
     * Dies ist eine der Grundlage für die Implementierung eigener
     * *Domain Specific Languages*.
     */
    @Test
    fun `04 Higher Order Functions schreiben`() {


        fun times(n: Int, f: () -> Unit) {

            for (i in 1..n)
                f()
        }



        times(3, { println("Hello") })


        times(2) { println("Moin") }


    }

    /**
     * ### Stream-Processing und der `it`-Parameter
     *
     * Hat ein Lambda nur einen Parameter, muss dieser im
     * Lambda nicht explizit deklariert werden, wenn der Typ
     * aus dem Kontext klar ist. Es darf dann `it` verwendet werden.
     */
    @Test
    fun `05 Der it-Parameter`() {

        val doppler: (Int) -> Int


        doppler = { it + it }


        assertThat(
                doppler(21)
        ).isEqualTo(42)


        "Hund ;KATZE; hund  ;  Maus;HUNd;;katze;katze;; hund;".split(";")
                .map { it.trim() }
                .filter { it.isNotEmpty() }
                .map { it.toLowerCase() }
                .groupBy { it.toString() }
                .mapValues { (word, occurences) -> occurences.size }
                .map { (word, count) -> "${count}x $word gefunden" }
                .forEach { println(it) }
    }


    /**
     * ### Referenzen auf Funktionen
     *
     * Auch Funktionen können auch wie Lambdas verwendet werden.
     * Mit der `::`-Notation erhält man Zugriff auf eine Funktion,
     * ohne sie sofort aufzurufen
     */
    @Test
    fun `06 Funktionsreferenzen`() {

        fun doppler(i: Int): Int = i + i

        val f =


                ::doppler


        assertThat(


                f(21)


        ).isEqualTo(42)


        // (Almost) *Point Free Style
        // -> Funktionen werden kombiniert, ohne vor Ort aufgerufen zu werden.

        fun format(word2count: Map.Entry<Any, Int>) =
                "${word2count.value}x ${word2count.key} gefunden"


        "Hund ;KATZE; hund  ;  Maus;HUNd;;katze;katze;; hund;".split(";")
                // .asSequence()
                .map(String::trim)
                .filterNot(String::isEmpty)
                .map(String::toLowerCase)
                .groupBy(String::toString)
                .mapValues { (word, occurences) -> occurences.size }
                .map(::format)
                .forEach(::println)
    }


    /**
     * Den [FizzBuzz-Test](http://wiki.c2.com/?FizzBuzzTest) ist zur Personalauswahl
     * zu verwenden ist wohl nur so eine mittelgute Idee. Trotzdem kann
     * FizzBuzz auch in Kotlin implementieren.
     */
    @Test
    fun `07 Schnörkellose FizzBuzz-Implementierung, nah der Spezifikation`() {
        (1..100)
                .forEach {
                    when {
                        it % 3 == 0 && it % 5 == 0 -> println("FizzBuzz")
                        it % 3 == 0 -> println("Fizz")
                        it % 5 == 0 -> println("Buzz")
                        else -> println(it)
                    }
                }
    }


    @Test
    fun `08 FizzBuzz mit mehr Schnörkeln und tabellarischer Ausgabeformatierung`() {

        fun isMultipleOf(n: Int, m: Int) = n.rem(m) == 0

        fun fizzBuzzify(n: Int) =
                when {
                    isMultipleOf(n, 3) && isMultipleOf(n, 5) -> "FizzBuzz"
                    isMultipleOf(n, 3) -> "Fizz"
                    isMultipleOf(n, 5) -> "Buzz"
                    else -> n.toString()
                }

        val columnsPerRow = 8
        val cellWidth = 10
        val headerRow = List(columnsPerRow) { "-".repeat(cellWidth) }
        fun padddingCells(n: Int) = List(n) { "" }

        (1..100) // Zelleninhalte erzeugen und formatieren
                .map(::fizzBuzzify)
                .map { " $it " }
                // Zeilen erzeugen und formatieren
                .chunked(columnsPerRow)
                .let { listOf(headerRow) + it + listOf(headerRow) }
                .map { it + padddingCells(columnsPerRow - it.size) }
                .map { it.map { it.padStart(cellWidth) } }
                .map { "|${it.joinToString("|")}|" }
                .forEach(::println)
    }

}

