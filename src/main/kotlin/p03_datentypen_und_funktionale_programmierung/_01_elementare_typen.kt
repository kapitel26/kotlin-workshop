package p03_datentypen_und_funktionale_programmierung

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.util.*

/**
 * ## Elementare Datentypen
 *
 * Ganzzahlen, Gleitkommazahlen, Zeichen und Boolesche Werte werden
 * in Kotlin durch Klassen dargestellt und können wie andere Objekte genutzt
 * werden. Aus Sicht des Programmierer gibt es keine besonderen
 * *primitiven Datentypen* (wie in Java).
 *
 */
class _01_elementare_typen {


    @Test
    fun `01 Int ist eine reguläre Klasse`() {

        val i = 42

        assertThat(


                i.toString()


        )
                .isEqualTo("42")
                .`as`("Auch zahlen haben member-Functions, wie z. B. toString()")

        val list = java.util.ArrayList<Int>()
        assertThat(


                list.add(i)


        )
                .isTrue()
                .`as`("Can be added to java collections that expecte Objects." +
                        "Without changing type as in java autoboxing.")

        assertThat(list.add(i))
    }


    /**
     * ## Ein klein wenig Reflection
     *
     * * Mit `::class` ermittelt man die Kotlin-Klasse
     * * Mit `is <Classname` kann man Klassenzugehörigkeit prüfen
     * * mit `.javaClass` erhält man Zugriff auf die Java-Klasse dazu
     *
     * Man sieht unter anderem, dass das Autoboxing von Java
     * unter der Haube verborgen bleibt.
     */
    @Test
    fun `02 Ein paar Typen, die in Java primitiv sind`() {

        assertThat(42::class.qualifiedName).isEqualTo("kotlin.Int")
        assertThat(42 is Int).isTrue()

        assertThat(42.toByte()::class.qualifiedName).isEqualTo("kotlin.Byte")
        assertThat(42.toByte().javaClass.name).isEqualTo("byte")

        assertThat("Hallo"[1]::class.qualifiedName).isEqualTo("kotlin.Char")
        assertThat("Hallo"[1].javaClass.name).isEqualTo("char")

        assertThat(true::class.qualifiedName).isEqualTo("kotlin.Boolean")
        assertThat(true.javaClass.name).isEqualTo("boolean")

        val unBoxed = 4.5
        assertThat(unBoxed::class.qualifiedName).isEqualTo("kotlin.Double")
        assertThat(unBoxed.javaClass.name).isEqualTo("double")

        val javaCollection = ArrayList<Double>()
        javaCollection.add(unBoxed)
        val boxed = javaCollection[0]

        assertThat(boxed::class.qualifiedName)
                .isEqualTo("kotlin.Double")
                .`as`("Für den Kotlin-Programmiere ist alles gleich geblieben.")
        assertThat(boxed.javaClass.name)
                .isEqualTo("java.lang.Double")
                .`as`("Für den Java-Programmerier hat die Zahl in der Collection" +
                        "den Typ gewechselt (Auto Boxing).")


    }

    /**
     * ## Die Spezialtypen Alles oder Nichts!
     *
     * Zwei Datentypen, die man nur selten direkt sieht:
     *
     *  * `Unit`
     *    * Hat nur einen Einzigen wert
     *    * **Jede** Funktion in Kotlin hat einen Rückgabetyp.
     *    * `Unit` wird verwendet, wo es auf die Rückgabe nicht ankommt.
     *    * Wir nur selten explizit angegeben
     *  * `Nothing`
     *    * Wird verwendet wenn eine Funktion nie Zurückkehren soll,
     *      z. B. eine Endlosschleife.
     *    * Muss aus Sicht des Compilers *unreachable* sind.
     */
    @Test
    fun `03 Die besonderen Typen Unit und Nothing`() {

        // sample functions declared below

        assertThat(::sayHello.returnType).hasToString("kotlin.Unit")
        assertThat(::sayBye.returnType).hasToString("kotlin.Unit")

        assertThat(::riverOfNoReturn.returnType).hasToString("kotlin.Nothing")
    }

    fun sayHello() {
        println("Hello")
    }

    fun sayBye(): Unit {
        println("Bye")
    }

    fun riverOfNoReturn(): Nothing {
        throw RuntimeException("Schluss")
    }
}
