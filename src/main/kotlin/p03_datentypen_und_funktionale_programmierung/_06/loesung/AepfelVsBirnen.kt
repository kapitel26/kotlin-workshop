package p03_datentypen_und_funktionale_programmierung._06.loesung

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Percentage.withPercentage
import org.junit.Test

/**
 * ### Übung: Hier werden Äpfel mit Birnen verglichen
 */
class AepfelVsBirnen {

    val verkaufsDatenClean = """
            Äpfel;2.11
            Äpfel;0.79
            Birnen;2.49
            Äpfel;3.43
            Birnen;0.99
    """.trimIndent().lines()

    data class UmsatzPosition(val artikel: String, val betrag: Double)

    @Test
    fun `_01 CSV Daten Parsen`() {


        val positionen: List<UmsatzPosition> =

                verkaufsDatenClean
                        // Parsen
                        .map { it.split(";") }
                        .map { (rawArtikel, rawBetrag) ->
                            UmsatzPosition(rawArtikel.trim(), rawBetrag.toDouble())
                        }

        assertThat(positionen)
                .hasSize(5)
                .startsWith(UmsatzPosition("Äpfel", 2.11))
                .endsWith(UmsatzPosition("Birnen", 0.99))


    }

    @Test
    fun `_02 Nach Artikel gruppieren und summieren`() {


        val gruppierteUmsaetze: Map<String, Double> =

                verkaufsDatenClean
                        // Parsen
                        .map { it.split(";") }
                        .map { (rawArtikel, rawBetrag) ->
                            UmsatzPosition(rawArtikel.trim(), rawBetrag.toDouble())
                        }
                        // Nach Artikel gruppieren und sortieren
                        .groupBy { it.artikel }
                        .mapValues { (artikel, positionen) ->
                            positionen
                                    .map { it.betrag }
                                    .sum()
                        }


        assertThat(gruppierteUmsaetze).hasSize(2)
        assertThat(gruppierteUmsaetze["Äpfel"]).isCloseTo(6.33, withPercentage(0.001))
        assertThat(gruppierteUmsaetze["Birnen"]).isCloseTo(3.48, withPercentage(0.001))

    }

    @Test
    fun `_03 Sortieren und Formatieren`() {

        fun formatUmsatz(artikel: String, summe: Double) =
                "%-12s %.2f €".format(artikel, summe)

        val sortierteAusgabe: List<String> =

                verkaufsDatenClean
                        // Parsen
                        .map { it.split(";") }
                        .map { (rawArtikel, rawBetrag) ->
                            UmsatzPosition(rawArtikel.trim(), rawBetrag.toDouble())
                        }
                        // Nach Artikel gruppieren und sortieren
                        .groupBy { it.artikel }
                        .mapValues { (artikel, positionen) ->
                            positionen
                                    .map { it.betrag }
                                    .sum()
                        }
                        // Sortieren und Formatieren
                        .toList()
                        .sortedBy { (artikel, summe) -> summe }
                        .map { (artikel, summe) -> formatUmsatz(artikel, summe) }

        sortierteAusgabe.forEach { println(it) }

        assertThat(sortierteAusgabe).containsExactly(
                "Birnen       3.48 €",
                "Äpfel        6.33 €"
        )

    }


    val verkaufsDatenDirty = """
            Äpfel;2.11
               Äpfel; 0.79
            Birnen; 2.49
            Birnen; 2.49; nix

            Birnen
            Äpfel;3.43
               Äpfel;1.11
            Birnen;0.99
    """.trimIndent().lines()

    @Test
    fun `04 Cleansing`() {

        fun formatUmsatz(artikel: String, summe: Double) =
                "%-12s %.2f €".format(artikel, summe)


        val (goodRows, badRows) =
                verkaufsDatenDirty
                        .map { it.split(";") }
                        .partition { it.size == 2 }

        val sortierteAusgabe: List<String> =
                goodRows
                        .map { (rawArtikel, rawBetrag) ->
                            UmsatzPosition(
                                    rawArtikel.trim(),
                                    rawBetrag.toDouble()
                            )
                        }


                        // Nach Artikel gruppieren und sortieren
                        .groupBy { it.artikel }
                        .mapValues { (artikel, positionen) ->
                            positionen
                                    .map { it.betrag }
                                    .sum()
                        }
                        // Sortieren und Formatieren
                        .toList()
                        .sortedBy { (artikel, summe) -> summe }
                        .map { (artikel, summe) -> formatUmsatz(artikel, summe) }
        sortierteAusgabe.forEach { println(it) }

        assertThat(sortierteAusgabe).containsExactly(
                "Birnen       3.48 €",
                "Äpfel        7.44 €"
        )

        assertThat(badRows).containsExactly(
                listOf("Birnen", " 2.49", " nix"),
                listOf(""),
                listOf("Birnen")
        )


    }

}
