package p07_ausblick

import io.ktor.util.random
import org.junit.Test

typealias Dings = String

class SandboxTest {

    @Test
    fun sandbox() {

        (1..5).windowed(3, partialWindows = true) { println(it) }
    }

}

fun main(args: Array<String>) {
    var hamlet = if( random(1) == 0 )  ToBe() else NotToBe()

    val theAnswer =
        when(hamlet) {
            is ToBe -> "Just be it: ${hamlet.zahl}"
            is NotToBe -> "Nö, doch nix."
            // The compiler will complain if a case is missing
        }

    println(theAnswer)
}

sealed class TheQuestion

class ToBe(val zahl: Int = 42) : TheQuestion()

class NotToBe() : TheQuestion()