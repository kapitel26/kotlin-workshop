# Quo Vadis Kotlin?

Kotlin hat Fahrt aufgenommen und entwickelt sich weiter. Wir werfen einen Blick auf Features und Entwicklungen, die in Sichtweite gerückt sind.

 * Allgemeine Verbesserungen
 * Funktionale Programmierung
 * Mulitplatform


## Allgemeine Verbesserung


### KEEP - Kotlin Evolution and Enhancement Process


Einen guten Überblick über die Entwiclung findet man auf KEEP.

 * [KEEP auf Slack](https://kotlinlang.slack.com/messages/language-proposals/)
 * [KEEP auf GitHub](https://github.com/Kotlin/KEEP)
   - Weiterentwicklung duch Diskussion und Pull-Requests
 * [KEEP Issues](https://github.com/Kotlin/KEEP/issues)


### Unterstützung der Weiterentwicklung


 * *Progressive Mode* (1.3)
 
   Schnellerer Fluß von Bugfixes
   
   >  passing the argument -progressive to the compiler.
   
   > Enabling the progressive mode can require you to rewrite some of your code, but it shouldn't be too much — 
   
   > nice choice for any actively maintained codebases

 * Experimental APIs (backlog)
 
   https://github.com/Kotlin/KEEP/blob/master/proposals/experimental.md
   
   Speed ist the key!
   

### "Aliasing"


Kotlin Unterstützt Aliase beim Import:

    import foo.For as Bar

Dies ist beispielsweise nützlich für Abkürzungen und zur Auflösung von Namenskonflikten.

    typealias GitHash = String
    
Zur Verbesserung der Lesbarkeit. Aber Achtung: Es handelt sich um eine simple Substitution. Im Beispiel oben ist jeder `String` auch `GitHash`.

Möchte man einen eigenen Typ `GitHash`, könnte man stattdessen ein Wrapper-Objeklt implementieren. Wrapper-Objekte bringen jedoch Overhead in Speicher und Delegation mit sich. Bei sehr häufig instanziierten oder verwendeten Objekten spielt das eine Rolle. Hier 

    inline class GitHash(private val value: String) { ... }

Inline-Klassen werden zur Laufzeit durch den enthaltenen Wert repräsentiert (ohne Wrapper). Trotzdem sorgt der Compilert für Typsicherheit und Verhalten.

ab 1.3  https://github.com/Kotlin/KEEP/blob/master/proposals/inline-classes.md

**Unsigned Integers** sind so implementiert.

ab 1.3  https://github.com/Kotlin/KEEP/blob/unsigned_types/proposals/unsigned-types.md



### Enhanced `main`


Man darf jetzt bei der `main`-Deklaration die `args` weglassen! 

> Dass ich das noch erleben darf! Nach 20 Jahren Java.

Und `suspend` darf man auch noch davor schreiben.

ab 1.3  https://github.com/Kotlin/KEEP/blob/enhancing-main-convention/proposals/enhancing-main-convention.md

       
### Sliding Window auf Collections


Nützlich! Z. B. in Data Science für Zeitreihenbewertung oder zur Implementierung von Algorithmen mit Lookahed/Prefetch.

    (1..5).windowed(3, partialWindows = true) { println(it) }  
    
    [1, 2, 3]
    [2, 3, 4]
    [3, 4, 5]
    [4, 5]
    [5]


ab 1.2         
       
       
### Coroutines (Release)


Jetzt auch als stable Release!
       
       
ab 1.3 https://github.com/Kotlin/KEEP/blob/master/proposals/coroutines.md
   
   
### Kotlin Contracts


ab 1.3  https://github.com/Kotlin/KEEP/blob/master/proposals/kotlin-contracts.md

Die Initialisierung von `i` im Beispiel unten war so bisher nicht zulässig, weil der Compiler nicht wissen konnte, wie oft die Funktion `run` den Block ausführt. Neuerding kann man *Hints* geben.
   
    val i: Int

    run {
        i = 42   // bisher unzulässig
    }

    [CallsInPlace(block, EXACTLY_ONCE)]
    fun run(block: () -> Unit) {
       ...
    }
    
    
Nützlich für hochwertige DSLs.


### Kotlin Script


Wird mit jedem Release ausgebaut.


Siehe Gradle-Build-Scripte in diesem Projekt.  


## Funktionale Programmierung
 
 
### FunKTionale
 
 
 - https://github.com/MarioAriasC/funKTionale
 - https://github.com/MarioAriasC/funKTionale/wiki


### Result - Encapsulating failure in Result (backlog)


   - https://github.com/Kotlin/KEEP/blob/master/proposals/stdlib/result.md


### Monsterfunktionen


ab 1. 3 big arity  https://github.com/Kotlin/KEEP/blob/master/proposals/functional-types-with-big-arity-on-jvm.md


### Sealed Classes - oder ein klein wenig ADT


Shakespeare in Kotlin:

    fun main(args: Array<String>) {
        var hamlet = if( random(1) == 0 )  ToBe() else NotToBe()
    
        val theAnswer =
            when(hamlet) {
                is ToBe -> "Just be it: ${hamlet.zahl}"
                is NotToBe -> "Nö, doch nix."
                // The compiler will complain if a case is missing
            }
    
        println(theAnswer)
    }
    
    sealed class TheQuestion
    
    class ToBe(val zahl: Int = 42) : TheQuestion()
    
    class NotToBe() : TheQuestion()


Der funktionale Programmierer freut sich über die *sealed Classes*, weil man mit Ihnen Algebraic Data Types kreieren kann.

https://kotlinlang.org/docs/reference/sealed-classes.html
ab 1.3 https://github.com/Kotlin/KEEP/blob/master/proposals/sealed-class-inheritance.md


## Multiplatform

 * Java-Script Demo
 
### Multiplatform Support

Frühere Ansätze (z. B. Java mit Swing) zielten darauf ab Plattformunabhängigkeit durch umfangreiche portable Umgebung (Engine + Libraries) zu erzielen. Dies führt zu einer (fast) vollständigen Entkopplung des laufenden Code von der Zielplattform.

Dem Design von Kotlin liegt die Annahme zugrunde, dass viele Anwendungen davon profitieren können, wenn sie die Zielplattform effektiv nutzen. Deshalb macht es Kotlin leicht, Libraries der Zielplattform (JVM, Javascript, Native) zu nutzen. Trotzdem möchte man natürlich gerne auch Libraries implementieren, die auf verschiedenen Plattformen genutzt werden können.

Deshalb bietet Kotlin Unterstützung dafür, solche Libraries zu implementieren.
Im API können Funktionen deklariert werden, deren Implementierung je nach Plattform eingesetzt wird.

    internal expect fun writeLogMessage(message: String, logLevel: LogLevel)

Eine Implementierung für die JVM könnte dann so aussehen:
    
    internal actual fun writeLogMessage(message: String, logLevel: LogLevel) {
        println("[$logLevel]: $message")
    }


https://kotlinlang.org/docs/reference/multiplatform.html
   
   
   
