package p05_dsl.s03_infix_operator_kodein.html

import kotlinx.html.div
import kotlinx.html.h1
import kotlinx.html.li
import kotlinx.html.stream.appendHTML
import kotlinx.html.ul

fun main(args: Array<String>) {
    System.out.appendHTML()
        .div {
            h1 {
                +"Features"
            }
            ul {
                li { +"Extension Functions" }
                li { +"Lambdas with receiver" }
                li { +"Operator overloading" }
            }
        }
}

