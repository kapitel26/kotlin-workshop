@file:Suppress("PackageDirectoryMismatch")

package p05_dsl.s01_infix_operator_delegation.operatorintro

class Circle(val x: Double, val y: Double, val radius: Double) :  Comparable<Circle> {

    infix fun intersects(other: Circle): Boolean {
        val distanceX = this.x - other.x
        val distanceY = this.y - other.y
        val radiusSum = this.radius + other.radius
        return distanceX * distanceX + distanceY * distanceY <= radiusSum * radiusSum
    }

    operator fun rem(other: Circle): Boolean = intersects(other)

    override fun compareTo(other: Circle): Int = (this.radius - other.radius).let {
        when {
            it < -0.0001 -> -1
            it > 0.0001 -> +1
            else -> 0
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Circle

        if (compareTo(other) != 0) return false

        return true
    }

    override fun hashCode(): Int {
        return radius.hashCode()
    }
}

fun main(args: Array<String>) {
    val daysInfix = mapOf("Mo" to "Monday", "Tu" to "Tuesday")
    val days = mapOf("Mo".to("Monday"), "Tu".to("Tuesday"))

    val c1 = Circle(x = 100.0, y = 100.0, radius = 50.0)
    val c2 = Circle(x = 75.0, y = 75.0, radius = 5.0)
    println(c1 % c2)
    println(c1 > c2)
    println(c1 == c2)
}