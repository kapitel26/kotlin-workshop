@file:Suppress("PackageDirectoryMismatch")

package p05_dsl.s03_infix_operator_kodein.kodein1

import org.h2.jdbcx.JdbcDataSource
import org.kodein.di.Kodein
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.generic.with
import javax.sql.DataSource

class DatabaseService(val dataSource: DataSource, val dbUrl: String)


fun main(args: Array<String>) {

    val kodein = Kodein {
        constant("dburl") with "jdbc:h2:mem:singleton"
        bind<DataSource>() with singleton { JdbcDataSource().apply { setURL(instance("dburl")) } }
        bind() from singleton { DatabaseService(instance(), instance("dburl")) }
    }

    val databaseService = kodein.direct.instance<DatabaseService>()
    println(databaseService.dbUrl)

}