package p05_dsl.w01_svg_dsl

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.html.respondHtml
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.body
import kotlinx.html.unsafe

class Circle(var cx: Int, var cy: Int, var r: Int) : SVGElement() {
    override fun generate(): String =
        """
            <circle cx="$cx" cy="$cy" r="$r" $style>
            </circle>
        """.trimIndent()
}

inline fun SVG.circle(cx: Int = 10, cy: Int = 10, r: Int = 10, body: Circle.() -> Unit = {}) {
    elements += Circle(cx, cy, r).apply(body)
}


fun Application.main() {
    install(Routing) {
        get("/") {
            val svg = svg {
                width = 300
                rect(10, 20) {
                    stroke = RGB(0, 1, 1)
                    fill = RGB(255, 0, 0)
                }
                circle {
                    cx = 30
                }
            }

            call.respondHtml {
                body {
                    unsafe {
                        raw(svg)
                    }
                }
            }
        }

    }
}

fun main(args: Array<String>) {
    val server = embeddedServer(
        Netty, port = 8080, module = Application::main
    )
    server.start(wait = true)
}