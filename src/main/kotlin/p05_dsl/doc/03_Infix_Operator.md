# Infix - Funktionen
* Funktionen die nur genau einen Parameter benötigen können als _Infix_-Funktionen definiert werden: 
z.B. die ``to``-Funktion um ein `Pair` zu erzeugen:
    ```kotlin
    val days = mapOf("Mo" to "Monday", "Tu" to "Tuesday")

    ```
    `to()` ist so definiert:
    ```kotlin
    infix fun <A, B> A.to(that: B): Pair<A, B> = Pair(this, that)

    ```
* Infix-Funktionen können die Lesbarkeit erhöhen, da weniger Klammern benötigt werden:
    ```kotlin
    val days = mapOf("Mo".to("Monday"), "Tu".to("Tuesday"))

    ```
    
* Eigene Infix Funktionen eigenen sich besonders im algebraischen Umfeld (siehe `s03_infix_operator_kodein/InfixIntro`):
    ```kotlin
    class Circle(val x: Double, val y: Double, val radius: Double) {
        infix fun intersects(other: Circle): Boolean {
            val distanceX = this.x - other.x
            val distanceY = this.y - other.y
            val radiusSum = this.radius + other.radius
            return distanceX * distanceX + distanceY * distanceY <= radiusSum * radiusSum
        }
    }

    val c1 = Circle(x = 100.0, y = 100.0, radius = 50.0)
    val c2 = Circle(x = 75.0, y = 75.0, radius = 5.0)
    println(c1 intersects c2)
    ```
    
# Kodein - Dependency Injection 
* [Kodein](https://github.com/Kodein-Framework/Kodein-DI) ist ein 
Dependency Injection Container und nutzt Infix-Funktionen um lesbareren Code zu erzeugen
(siehe `s03_infix_operator_kodein/KodeinExample1` ).

```kotlin
val kodein = Kodein {
    constant("dburl") with "jdbc:h2:mem:singleton"
    bind<DataSource>() with singleton { JdbcDataSource().apply { setURL(instance("dburl")) } }
    bind() from singleton { DatabaseService(instance(), instance("dburl")) }
}
```        
        
# Operatoren redefinieren
* Kotlin erlaubt das Redefinieren von vorhandenen [Operatoren](https://kotlinlang.org/docs/reference/operator-overloading.html). 
  Das Definieren von ganz neuen Operatoren ist nicht möglich.
* Operatoren zu redefinieren kann in algebraischen Domänen sinnvoll sein.
  Missbrauch führt zu schwer verständlichen Code und sollte besser durch eigene Infix-Funktionen ausgetauscht werden
  (siehe `s01_infix_operator_delegation/OperatorIntro`).
  
    ```kotlin
    class Circle(val x: Double, val y: Double, val radius: Double) {
        infix fun intersects(other: Circle): Boolean {
            val distanceX = this.x - other.x
            val distanceY = this.y - other.y
            val radiusSum = this.radius + other.radius
            return distanceX * distanceX + distanceY * distanceY <= radiusSum * radiusSum
        }
    
        operator fun rem(other: Circle): Boolean = intersects(other)
    }


    val c1 = Circle(x = 100.0, y = 100.0, radius = 50.0)
    val c2 = Circle(x = 75.0, y = 75.0, radius = 5.0)
    println(c1 % c2)
    ```
* Einige Standard-Operatoren werden auch automatisch in entsprechende Methoden übersetzt, z.B.`<`, `>` zu `compareTo` und `==` zu `equals`
  (siehe `s01_infix_operator_delegation/OperatorIntro`)
         

## Reale Beispiele
* `kotlinx.html` (siehe `s03_infix_operator_kodein.html/KotlinxHtmlOperatorExample`)

    ```kotlin
    h1 {
        +"Features"
    }
    ul {
        li { +"Extension Functions" }
        li { +"Lambdas with receiver" }
        li { +"Operator overloading" }
    }
    ```

* `gradle` (siehe  `build.gradle.kts`)    
    ```kotlin
    tasks {        
        "hello" {
            doLast {
                println("Hello again")
            }
        }
    }
    ```