# Delegated Properties
* _Delegated Properties_ erlauben es die Getter und Setter von Properties über ein anderes Objekt zu delegieren und
  dabei Funktionalität hinzuzufügen. Das Schlüsselwort ist `by`. (siehe `s02_delegation_argsparser/DelegationIntro`).
  
    ```kotlin
    class Circle(val x: Double, val y: Double, val radius: Double) {
        val area: Double by lazy {
            PI * radius * radius
        }
    }
    ```      

* Delegation Objekte müssen spezielle Operatoren `getValue` und  ggf. `setValue` definieren, z.B. das `Lazy`-Interface  
    
    ```kotlin
    interface Lazy<T> {
       val value: T

       operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value
    }
    ```
* Entscheidend ist dabei, das die Funktionen auch ein `KProperty`-Objekt bekommen, in dem unter anderen der Name des Properties
  enthalten ist. Damit ist z.B. der Zugriff auf Maps möglich, die die entsprechenen Operatoren implementieren 
  (siehe `s02_delegation_argsparser/DelegationMap`).
  
    ```kotlin
    class Person(data: Map<String, Any?>) {
      val firstname: String by data
      val surname: String by data
      val eMail: String? by data
    }
    ```

* Ein Beispiel für ein eigenes _Delegated Property_ ist unter  `s02_delegation_argsparser/DelegationTracing` zu finden.           

# ArgParser als Beispiel für _Delegated Properties_
* [ArgParser](https://github.com/xenomachina/kotlin-argparser) ist eine kleine Bibliothek um Kommandozeilenparameter zu parsen
und Hilfe bzw. Fehlermeldungen auszugeben (siehe `s02_delegation_argsparser/ArgsParserExample1`).

```kotlin
class RenamerArgs(parser: ArgParser) {
    val dir by parser.positional("start directory")

    val fromChar by parser.positional<Char>("char to remove") {
        get(0)
    }

    val toChar by parser.positional<Char>("char to insert") {
        get(0)
    }

    val f by parser.flagging("rename really")
}
```

# Delegation Interfaces
* Man kann auch ganze Interfaces delegieren. Dazu gibt man bei der Vererbung mit dem Schlüsselwort `by` das Delegation-Objekt an
(siehe `s02_delegation_argsparser/DelegationInterface`).
    
    ```kotlin
    class Person(data: MutableMap<String, Any?>) : MutableMap<String, Any?> by data {
        val firstname: String by data
        val surname: String by data
        val eMail: String? by data
    }

   person["firstname"] = "Hans"
    ```