# DSL-Beipiele
## `kotlinx.html` 
* `kotlinx.html` ist eine typsichere Builder-Api zum Erzeugen von HTML.
(siehe `s01_kotlinx_html_ktor/KotlinxHtmlExample)`)

* Lambda With Receiver bildet dabei die  Grundlage für eine Builder-DSL: 

    ```kotlin
     System.out.appendHTML()
            // Lambda with Receiver
            .div {
                h1 {
                    text("Features")
                }
                ul {
                    li { text("Extension Functions") }
                    li { text("Lambdas with receiver") }
                    li { text("Operator overloading") }
                }
            }
    ```    

* Extension Funktionen ermöglichen es die DSL einfach zu erweitern

    ```kotlin
    fun FlowOrInteractiveOrPhrasingContent.menuEntry(title: String, href: String) =
        a(href) {
            h2 { text(title) }
        }
      
     System.out.appendHTML()
            .div {
                //Aufruf mit Named-Parameter macht den Code lesbarer
                menuEntry(title = "Menu 1", href = "./link1")
                menuEntry(title = "Menu 2", href = "./link2")
            }
    ```
    
* Die `DSLMarker`-Annotation verhindert das Aufrufen von _äusseren_ Funktionen

    ```kotlin
    ul {
        li {
            li { // error
    
            } 
        }
    }
    ```     

* Eine markierte Funktion kann keine gleichartig markierte Funktion an einem _äusseren_ `this`-Zeiger aufrufen.
  Man kann auch ganze Klassen annotatieren.    
    
    ````kotlin
    @DslMarker
    annotation class HtmlTagMarker

    @HtmlTagMarker
    fun UL.li(classes : String? = null, block : LI.() -> Unit = {}) : Unit = LI(attributesMapOf("class", classes), consumer).visit(block)

    ````
    
## Ktor -  Webframework
* Ktor ist ein asynchrones Server-Framework um Webanwendngen bzw. APIs umzusetzen. Ktor nutzt Extension-Funktionen anstelle von Vererbung.
  (siehe `s01_kotlinx_html_ktor/KtorExample1)`)    

    ```kotlin
    fun Application.main() {
        install(Routing) {
        }
    }
    ``` 
* Lambdas With Receiver werden für die Router-Api genutzt.
    
    ```kotlin
    fun Application.main() {
        install(Routing) {
              get("/hello1") {
                  call.respondTextWriter {
                      appendln("Hello")
                  }
              }
        }
    }
    ```
    
* Modularisierung des Frameworks ist mittels Extensions umgesetzt (siehe `s01_kotlinx_html_ktor/KtorExample2)`).     

    ```kotlin
    //Extension-Funktion für Route
    get<Index> { 
        ...
        //Extension-Property an PipelineContext
        locations.href(Hello("Rene"))
        ...
    }
    ```