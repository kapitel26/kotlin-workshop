@file:Suppress("PackageDirectoryMismatch") @file:JvmName("KtorExample2Kt")

package p05_dsl.s01_kotlinx_html_ktor.ktor1

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondTextWriter
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.experimental.reactor.mono

fun Application.main() {
    install(Routing) {
        get("/hello1") {
            call.respondTextWriter {
                appendln("Hello")
            }
        }

        get("/hello2/{name}") {
            val name = call.parameters["name"]
            if (name == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                call.respondTextWriter {
                    appendln("Hello $name")
                }
            }
        }
    }
}


fun main(args: Array<String>) {
    val server = embeddedServer(
        Netty, port = 8080, module = Application::main
    )
    server.start(wait = true)
}