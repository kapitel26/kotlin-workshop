@file:Suppress("PackageDirectoryMismatch")

package p05_dsl.s02_delegation_argsparser.delegationintro

import kotlin.math.PI

class Circle(val x: Double, val y: Double, val radius: Double) {
    val area: Double by lazy {
        PI * radius * radius
    }
}

fun main(args: Array<String>) {
    val circle = Circle(1.0, 2.0, 3.0)
    println(circle.area)
}