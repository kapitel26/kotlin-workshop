@file:Suppress("PackageDirectoryMismatch")

package p05_dsl.s02_delegation_argsparser.delegationexample

import kotlin.reflect.KProperty

class Tracer<T>(var value: T) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        println("In ${thisRef?.javaClass?.simpleName} read - ${property.name} : $value")
        return value
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, newValue: T) {
        println("In ${thisRef?.javaClass?.simpleName} write - ${property.name} : $value -> $newValue")
        value = newValue
    }

}

fun <T> tracing(init: T): Tracer<T> = Tracer(init)


class Circle(x: Double, y: Double, radius: Double) {
    var x by tracing(x)
    var y by tracing(y)
    var radius by tracing(radius)
}

fun main(args: Array<String>) {
    val c = Circle(1.0, 2.0, 3.0)
    c.radius = 4.0
}