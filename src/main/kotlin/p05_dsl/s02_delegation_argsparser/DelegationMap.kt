@file:Suppress("PackageDirectoryMismatch")

package p05_dsl.s02_delegation_argsparser.delegationmap


class Person(data: Map<String, Any?>) {
    val firstname: String by data
    val surname: String by data
    val eMail: String? by data
}

fun main(args: Array<String>) {
    val data = mapOf("firstname" to "Rene", "surname" to "Preißel")

    val person = Person(data)
    with(person) {
        println("$firstname $surname")
    }
}