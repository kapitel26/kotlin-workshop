@file:Suppress("PackageDirectoryMismatch")

package p05_dsl.s02_delegation_argsparser.delegateioninterface


class Person(data: MutableMap<String, Any?>) : MutableMap<String, Any?> by data {
    val firstname: String by data
    val surname: String by data
    val eMail: String? by data
}

fun main(args: Array<String>) {
    val person = Person(mutableMapOf("firstname" to "Rene", "surname" to "Preißel"))

    println(person.firstname)
    person["firstname"] = "Hans"
    println(person.firstname)
}