@file:Suppress("PackageDirectoryMismatch")

package p05_dsl.s02_delegation_argsparser.parser1


import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.SystemExitException
import java.io.File

class RenamerArgs(parser: ArgParser) {
    val dir by parser.positional("start directory")

    val fromChar by parser.positional<Char>("char to remove") {
        get(0)
    }

    val toChar by parser.positional<Char>("char to insert") {
        get(0)
    }

    val force by parser.flagging("rename really")
}


fun listFilesRecursively(dir: File, filter: (File) -> Boolean): Sequence<File> {
    val (allSubDirs, filteredFiles) = dir.listFiles { f: File ->
        f.isDirectory || filter(f)
    }.partition { it.isDirectory }

    return filteredFiles.asSequence() + allSubDirs.asSequence().flatMap { listFilesRecursively(it, filter) }
}

fun main(args: Array<String>) {
    val parsedArgs = try {
        ArgParser(args).parseInto(::RenamerArgs)
    } catch (e: SystemExitException) {
        e.printAndExit()
    }

    println("${if (parsedArgs.force) "Run rename:" else "Dry run rename:"}  '${parsedArgs.fromChar}' to '${parsedArgs.toChar}' in dir ${parsedArgs.dir}")

    val listFiles =
        listFilesRecursively(File(parsedArgs.dir)) {
            it.nameWithoutExtension.contains(parsedArgs.fromChar)
        }
    for (file in listFiles) {
        val newName = File(
            file.parent,
            "${file.nameWithoutExtension.replace(parsedArgs.fromChar, parsedArgs.toChar)}.${file.extension}"
        )
        if (parsedArgs.force) {
            val success = file.renameTo(newName)
            if (success)
                println("Rename $file to $newName")
            else
                System.err.println("Could not rename $file to $newName")
        } else {
            println("Would rename $file to $newName")
        }
    }
}
