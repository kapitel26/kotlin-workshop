## Package Deklaration

Wie in Java kann man eine Datei mit einer `package`-Deklaration beginnen, um bestimmen, in welches Package nachfolgend definierte Klassen und Funktionen einzuordnen sind.

 * In Kotlin ist, anders als in Java, die Ablage der Source-Files unabhängig von der Package-Struktur.
 
 * Konvention:
 
   - In gemischten Projekten (Java+Kotlin) werden Kotlin-Files in die Java-Struktur eingeordnet.
   
   - In Kotlin-Projekten bildet man das gemeinsame  *Prefix* nicht in der Verzeichnisstruktur ab:
   
     - aus den Packages
         ```
         de.kapitel26.supertool.foo
         de.kapitel26.supertool.blah.dings
         ```
     - werden die Source-Folder
        ```
        foo/
        blah/dings
        ```
       
   