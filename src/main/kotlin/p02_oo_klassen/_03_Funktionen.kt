package p02_oo_klassen

import org.junit.Assert.assertTrue
import org.junit.Test
import java.lang.System.currentTimeMillis
import java.lang.System.nanoTime
import java.util.*
import java.util.Calendar.*

/**
 * ## Funktionen in Kotlin
 *
 * Kotlin-*Funktionen* entsprechen im Wesentlichen den *Methoden* von Java.
 *
 * Folgende Dinge sind in Kotlin neu/anders/besser:
 *
 * - `fun`-Keyword (mandatory)
 *
 * - *Prozeduren* ohne `void` deklarieren
 *
 * - *Einzelausdrücke*
 *
 *    Mit `=` kann ein einzelner Audruck/Anweisung zugewiesen werden (statt eines Block)
 *
 * - Verschachtelte Deklaration möglich
 *   (gut für Hilfsfunktionen).
 *
 * - Methodennamen erlauben Leerzeichen
 *   (gut für Namen von Unit-Tests)
 *
 * - *Destructuring*
 *
 *    Zuweisung aus einem Ausdruck an mehrere Variablen
 *    (kürzt Code bei Rückgabe zusammengesetzter Datentypen)
 *
 * - Optionale Parameter mit Default-Werten
 *
 *    (erspart für java typische redundate Signaturdeklarationen)
 *
 * - Parameterübergabe an *benannte Parameter*
 *    (erhöht Lesbarkeit, besonders bei Funktionen mit vielen Parametern)
 *
 * ### Fazit
 *
 * > Kotlin-Programmierer haben mehr **`fun`**.
 */
class _03_Funktionen {

    /**
     * ## Einfache Prozedur deklarieren
     *
     *  * Keyword `fun`
     *  * Aufruf *immer* mit `()`-Paramterklammern!
     */
    @Test
    fun `01 Mit einem Block von Ausdrücken`() {

        // Deklaration

        fun sayHello() {
            println("Hello")
            println("World")
        }

        // Aufruf

        sayHello()
    }

    /**
     * ## Funktion mit nur einem Ausdruck/einer Anweisung
     *
     * Der Einzelausdruck darf mit `=` zugewiesen werden
     */
    @Test
    fun `01 Funktion aus einem Ausdruck deklarieren`() {

        fun sayHello() = println("Hello World!")

        sayHello()
    }

    /**
     * ## Rückgabewerte von Funktionen
     *
     * * Typangabe wird mit `:` an die Deklaration angehängt
     * * Besteht die Funktion aus einem Block,
     *   **muss** ein `return` für die Rückgabe verwendet werden.
     */
    @Test
    fun `03 Rückgabewerte`() {

        fun now(): Long = currentTimeMillis()

        fun nanos(): Long {
            return nanoTime()  // Im Block return erforderlich!
        }

        println("millis = ${now()}")
        println("Nanos = ${nanos()}")

    }

    /**
     * ## Type-Inference für Rückgabewerte
     *
     * Typangaben dürfen (und sollten) in Kotlin oft weggelassen werden,
     * wenn Sie sich aus dem Kontext ergeben.
     */
    @Test
    fun `05 Typ der Rückgabe muss nicht deklariert werden, wenn durch Return oder Zuweisung ermittelbar`() {

        fun nanos() = nanoTime()

        assertTrue(nanos() is Long)
    }

    /**
     * ## Mehrfachzuweisung mit Destructuring
     *
     * Wenn eine Funktion ein zusammengesetztes Objekt liefert,
     * hier z. B. ein [Pair],
     * kann man dessen Bestandteile in einer Zeile
     * mehreren Variablen zuweisen.
     */
    @Test
    fun `06 Zusammengesetzter Rückgabewert mit Destructuring`() {

        fun detailedNow(): Pair<Long, Long> =

                Pair(currentTimeMillis(), nanoTime())

        // Destructuring

        val (millis, nanos) = detailedNow()


        println("millis=$millis nanos=$nanos")
    }

    /**
     * ## Funktion mit einfachen Parametern
     *
     * Die Deklaration einfacher Parameter erfolgt wie in Java,
     * nur dass die Typangabe mit `:` hinter den Parameternamen gestellt wird.
     */
    @Test
    fun `07 Parameter (einfach)`() {


        fun printSinglePrice(article: String, price: Double) =

                println("1x $article á $price€")


        printSinglePrice("Avocado", 0.49)

    }

    /**
     * ## Default-Werte für optionale Parameter
     *
     * Kotlin unterstützt, anders als Java, *optionale Parameter*.
     *
     * * Bei der Parameter Deklaration wird gleich ein Wert zugewiesen werden
     *
     * * Der (oder die) letzten Optionalen Parameter dürfen
     *   beim Aufruf weggelassen werden.
     *
     * * Für weggelassene Parameter gilt der oben angegebene Default-Wert.
     *
     */
    @Test
    fun `08 Parameter mit Defaultwerten`() {


        fun printPrice(
                article: String,
                price: Double,
                currency: String = "€",
                number: Int = 1
        ) =
                println(
                        "${number}x $article" +
                                " á $price ${currency} " +
                                "= ${price * number}$currency"
                )


        printPrice("Avocado", 0.49, "$", 3)
        printPrice("Avocado", 0.49, "$")
        printPrice("Avocado", 0.49)
    }

    /**
     * ## Funktionsaufruf mit benannten Parametern
     *
     *
     */
    @Test
    fun `09 Funktionsaufruf mit benannten Parametern`() {


        printPrice(
                "Avocado",
                0.49, number = 3
        )
        printPrice(
                number = 3,
                article = "Gemüsezwiebel",
                price = 0.11)
    }

    fun printPrice(
            article: String,
            price: Double,
            currency: String = "€",
            number: Int = 1
    ) = println(
            "${number}x $article á $price ${currency} = ${price * number}$currency"
    )

    /**
     * ## Das `when`-Statement
     *
     * Das `when`-Statement stellt Fallunterscheidungen mit mehr als
     * zwei Zweigen übersichtlicher dar, als eine Folge von `if`-Statements.
     *
     * **Hinweis** Das `when`-Statemtent in Kotlin ist sehr mächtig,
     * z. B. bei Verwendung von `in`- oder `is`-Bedingungen
     * Hier werden nur ein paar einfache Fälle angerissen.
     */
    @Test
    fun `10 das when statement kann Fallunterscheidungen in Funktionen lesbarer machen`() {

        fun fib(n: Int): Int =

        // Fallunterscheidung über einen Parameter, hier `n`

                when (n) {
                    1 -> 1
                    2 -> 1
                    else -> fib(n - 1) + fib(n - 2)
                }

        println("fib(5): ${fib(5)}")


        val hour = Calendar.getInstance().get(HOUR_OF_DAY)
        val day = Calendar.getInstance().get(DAY_OF_WEEK)

        // Fallunterscheidung über freie Bedingungen

        when {
            hour == 17 ->
                println("Tea Time")

            hour == 11 && day == SUNDAY ->
                println("Brunch")

            else ->
                println("Weiss nicht recht")
        }

        when (hour) {
            in 8..11 -> println("Vormittag")
            in 12..18 -> println("Nachmittag")
            else -> println("Finstere Nacht")
        }
    }
}
