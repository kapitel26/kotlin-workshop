package p02_oo_klassen._08b.java;



public class GeldBetrag {

    // Fields

    private long inCent;

    // Constructors

    private GeldBetrag(long inCent) {
        this.inCent = inCent;
    }

    public GeldBetrag(double betrag) {
        this((long) (betrag * 100.0));
    }


    // Getter and Setter

    public long getInCent() {
        return inCent;
    }


    // Custom Methods

    public GeldBetrag mal(int n) {
        return new GeldBetrag(inCent * n);
    }

    public GeldBetrag plus(GeldBetrag b) {
        return new GeldBetrag(inCent + b.inCent);
    }

    public QuotientMitRest teile(int divisor) {
        return new QuotientMitRest(
                new GeldBetrag(inCent / divisor),
                new GeldBetrag(inCent % divisor)
        );
    }

    // Standard Methods

    @Override
    public String toString() {
        return String.format("€ %.2f", inCent / 100.0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeldBetrag that = (GeldBetrag) o;
        return inCent == that.inCent;
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(inCent);
    }

}