package p02_oo_klassen._08b._loesung

import kotlin.math.round


data class QuotientMitRest(val quotient: GeldBetrag, val rest: GeldBetrag)

/**
 * ### Musterlösung: Geld Teilen
 *
 */
data class GeldBetrag(val inCent: Long) {

    constructor(betrag: Double)
            : this(round(betrag * 100.0).toLong())

    fun mal(n: Int) = GeldBetrag(inCent * n)

    fun plus(b: GeldBetrag) = GeldBetrag(inCent + b.inCent)

    fun teile(divisor: Int) =
            QuotientMitRest(
                    GeldBetrag(inCent / divisor),
                    GeldBetrag(inCent % divisor)
            )

    override fun toString(): String = "€ %.2f".format(inCent / 100.0)
}


