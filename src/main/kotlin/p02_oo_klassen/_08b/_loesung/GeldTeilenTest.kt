package p02_oo_klassen._08b._loesung

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


/**
 * ### Musterlösung: Geld Teilen
 */
class GeldTeilenTest {


    @Test
    fun `Teilen mit Rest`() {
        val b = GeldBetrag(47.11)

        assertThat(b.inCent).isEqualTo(4711)

        assertThat(b.mal(2).inCent).isEqualTo(9422)
        assertThat(b.plus(GeldBetrag(0.89)).inCent).isEqualTo(4800)

        assertThat(GeldBetrag(47.00).plus(GeldBetrag(0.11)))
                .isEqualTo(b)

        val (quotient, rest) = b.teile(4)

        assertThat(quotient).isEqualTo(GeldBetrag(11.77))
        assertThat(rest).isEqualTo(GeldBetrag(0.03))

        assertThat(quotient.mal(4).plus(rest))
                .isEqualTo(b)
    }

}