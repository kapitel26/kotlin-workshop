package p02_oo_klassen._09b.uebung;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static p02_oo_klassen._09b.java.BruchUtils.ggT;

public class BruchTest {

    @Test
    public void konstruktorZaehlerUndNenner() {
        Bruch b = new Bruch(3, 7);

        assertEquals(b.getZaehler(), 3);
        assertEquals(b.getNenner(), 7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void konstruktorZaehlerUndNennerException() {
        new Bruch(3, 0);
    }

    @Test
    public void bruchWirdGekuerzt() {
        Bruch b = new Bruch(2, 6);

        assertEquals(b.getZaehler(), 1);
        assertEquals(b.getNenner(), 3);
    }

    @Test
    public void konstruktor0Nenner() {
        Bruch b = new Bruch(0, 8);

        assertEquals(b.getZaehler(), 0);
        assertEquals(b.getNenner(), 1);
    }

    @Test
    public void konstruktorBruchWirdGekuerztNegativ() {
        Bruch b = new Bruch(-6, 8);

        assertEquals(b.getZaehler(), -3);
        assertEquals(b.getNenner(), 4);
    }

    @Test
    public void gleichheitVonBruechen() {
        assertEquals(new Bruch(2, 6), new Bruch(1, 3));
        assertEquals(new Bruch(4, 6), new Bruch(6, 9));
        assertEquals(new Bruch(42, 132), new Bruch(7, 22));
        assertEquals(new Bruch(-5, 6), new Bruch(-10, 12));
    }

    @Test
    public void ggtBerechnung() {
        assertEquals(ggT(1, 1), 1);
        assertEquals(ggT(23, 23), 23);
        assertEquals(ggT(4, 4), 4);

        assertEquals(ggT(2, 6), 2);
        assertEquals(ggT(6, 2), 2);

        assertEquals(ggT(49, 7), 7);
        assertEquals(ggT(7, 49), 7);

        assertEquals(ggT(3 * 11 * 23, 11 * 37), 11);
        assertEquals(ggT(1, 23), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ggTException1() {
        ggT(0, 7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ggTException2() {
        ggT(7, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ggTException3() {
        ggT(-11, 7);
    }

}
