package p02_oo_klassen._09b.java;

public abstract class BruchUtils {

    public static long ggT(long a, long b) {
        if (a < 1)
            throw new IllegalArgumentException("a out of range:" + a);

        if (b < 1)
            throw new IllegalArgumentException("a out of range:" + a);

        if (a == b)
            return a;
        else if (a > b)
            return ggT(a - b, b);
        else
            return ggT(b - a, a);
    }

}
