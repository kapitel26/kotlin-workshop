package p02_oo_klassen._09b.java;

import java.util.Objects;

import static java.lang.Math.abs;

public class Bruch {

    private long zaehler;
    private long nenner;

    public Bruch(long zaehler, long nenner) {
        if (nenner < 1)
            throw new IllegalArgumentException("Nenner nicht positiv: " + nenner);

        if (zaehler == 0) {
            this.zaehler = 0;
            this.nenner = 1;
        } else {
            long ggT = BruchUtils.ggT(abs(zaehler), nenner);

            this.zaehler = zaehler / ggT;
            this.nenner = nenner / ggT;
        }
    }

    public long getZaehler() {
        return zaehler;
    }

    public long getNenner() {
        return nenner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bruch bruch = (Bruch) o;
        return zaehler == bruch.zaehler &&
                nenner == bruch.nenner;
    }

    @Override
    public int hashCode() {
        return Objects.hash(zaehler, nenner);
    }

    @Override
    public String toString() {
        return "Bruch{" +
                "zaehler=" + zaehler +
                ", nenner=" + nenner +
                '}';
    }

}
