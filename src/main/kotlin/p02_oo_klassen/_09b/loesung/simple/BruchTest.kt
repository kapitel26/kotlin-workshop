package p02_oo_klassen._09b.loesung.simple

import org.junit.Assert.assertEquals
import org.junit.Test
import p02_oo_klassen._09b.loesung.simple.BruchUtils.ggT

class BruchTest {

    @Test
    fun konstruktorZaehlerUndNenner() {
        val b = Bruch(3, 7)

        assertEquals(b.zaehler, 3)
        assertEquals(b.nenner, 7)
    }

    @Test(expected = IllegalArgumentException::class)
    fun konstruktorZaehlerUndNennerException() {
        Bruch(3, 0)
    }

    @Test
    fun bruchWirdGekuerzt() {
        val b = Bruch(2, 6)

        assertEquals(b.zaehler, 1)
        assertEquals(b.nenner, 3)
    }

    @Test
    fun konstruktor0Nenner() {
        val b = Bruch(0, 8)

        assertEquals(b.zaehler, 0)
        assertEquals(b.nenner, 1)
    }

    @Test
    fun konstruktorBruchWirdGekuerztNegativ() {
        val b = Bruch(-6, 8)

        assertEquals(b.zaehler, -3)
        assertEquals(b.nenner, 4)
    }

    @Test
    fun gleichheitVonBruechen() {
        assertEquals(Bruch(2, 6), Bruch(1, 3))
        assertEquals(Bruch(4, 6), Bruch(6, 9))
        assertEquals(Bruch(42, 132), Bruch(7, 22))
        assertEquals(Bruch(-5, 6), Bruch(-10, 12))
    }

    @Test
    fun ggtBerechnung() {
        assertEquals(ggT(1, 1), 1)
        assertEquals(ggT(23, 23), 23)
        assertEquals(ggT(4, 4), 4)

        assertEquals(ggT(2, 6), 2)
        assertEquals(ggT(6, 2), 2)

        assertEquals(ggT(49, 7), 7)
        assertEquals(ggT(7, 49), 7)

        assertEquals(ggT((3 * 11 * 23).toLong(), (11 * 37).toLong()), 11)
        assertEquals(ggT(1, 23), 1)
    }

    @Test(expected = IllegalArgumentException::class)
    fun ggTException1() {
        ggT(0, 7)
    }

    @Test(expected = IllegalArgumentException::class)
    fun ggTException2() {
        ggT(7, 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun ggTException3() {
        ggT(-11, 7)
    }

}
