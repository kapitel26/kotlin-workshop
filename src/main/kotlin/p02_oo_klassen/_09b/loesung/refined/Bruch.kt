package p02_oo_klassen._09b.loesung.refined

import java.lang.Math.abs


data class Bruch(val zaehler: Long, val nenner: Long)


fun bruch(zaehler: Long, nenner: Long) =
        when {
            nenner < 1 ->
                throw IllegalArgumentException("Nenner nicht positiv: $nenner")
            zaehler == 0L ->
                Bruch(0, 1)
            else -> {
                val ggT = ggT(abs(zaehler), nenner)
                Bruch(zaehler / ggT, nenner / ggT)
            }
        }


fun ggT(a: Long, b: Long): Long =
        when {
            a < 1 -> throw IllegalArgumentException("a out of range:$a")
            b < 1 -> throw IllegalArgumentException("b out of range:$a")
            a == b -> a
            a > b -> ggT(a - b, b)
            else -> ggT(b - a, a)
        }
