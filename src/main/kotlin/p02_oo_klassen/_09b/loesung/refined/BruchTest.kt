package p02_oo_klassen._09b.loesung.refined


import org.junit.Assert.assertEquals
import org.junit.Test

class BruchTest {

    @Test
    fun `Konstruktur setzt Zähler und Nenner`() {

        with(bruch(3, 7)) {
            assertEquals(zaehler, 3)
            assertEquals(nenner, 7)
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `Nenner 0 ist unzulässig`() {

        bruch(3, 0)
    }

    @Test
    fun `Konstruktor kürzt, wo notwendig`() {

        with(bruch(2, 6)) {
            assertEquals(zaehler, 1)
            assertEquals(nenner, 3)
        }
    }

    @Test
    fun `0-Brüche werden normiert`() {

        with(bruch(0, 8)) {
            assertEquals(zaehler, 0)
            assertEquals(nenner, 1)
        }

    }

    @Test
    fun `Auch bei negativen Zahlen wird korrekt gekürzt`() {

        with(bruch(-6, 8)) {
            assertEquals(zaehler, -3)
            assertEquals(nenner, 4)
        }

    }

    @Test
    fun `Brüche werden als gleich erkannt, wenn unterschiedliche ungekürzte Repräsentationen genutzt werden`() {
        assertEquals(bruch(2, 6), bruch(1, 3))
        assertEquals(bruch(4, 6), bruch(6, 9))
        assertEquals(bruch(42, 132), bruch(7, 22))
        assertEquals(bruch(-5, 6), bruch(-10, 12))
    }

    @Test
    fun `Größter gemeinsamer Teiler, wird korrekt berechnet`() {
        assertEquals(ggT(1, 1), 1)
        assertEquals(ggT(23, 23), 23)
        assertEquals(ggT(4, 4), 4)

        assertEquals(ggT(2, 6), 2)
        assertEquals(ggT(6, 2), 2)

        assertEquals(ggT(49, 7), 7)
        assertEquals(ggT(7, 49), 7)

        assertEquals(ggT((3 * 11 * 23).toLong(), (11 * 37).toLong()), 11)
        assertEquals(ggT(1, 23), 1)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `Nur Positive Werte sind zulässig(1)`() {
        ggT(0, 7)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `Nur Positive Werte sind zulässig(2)`() {
        ggT(7, 0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `Nur Positive Werte sind zulässig(3)`() {
        ggT(-11, 7)
    }

}
