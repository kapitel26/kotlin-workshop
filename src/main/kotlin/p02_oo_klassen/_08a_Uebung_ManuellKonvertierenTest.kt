package p02_oo_klassen

import org.junit.Test
import p02_oo_klassen._08b.uebung.GeldBetrag

/**
 * ## Übung: Portierung von Java-Klassen
 *
 * Es geht darum,
 * die Java-Klasse `GeldBetrag` ([p02_oo_klassen._08b.java.GeldBetrag]) und
 * die Hilfsklasse `QuotientMitRest` ([p02_oo_klassen._08b.java.QuotientMitRest])
 * nach Kotlin zu portieren.
 *
 * 1. Reaktiviere den auskommentierten Code im Test [Teilen mit Rest].
 *    - Der Test sollte jetzt **rot** sein.
 *
 * 1. Portiere die beiden Java-Klassen
 *
 * 1. Mache weiter bis der Test **grün** ist.
 *
 */
class _08a_Uebung_ManuellKonvertierenTest {

    @Test
    fun `Teilen mit Rest`() {
        println("Teste die portierte Klasse: ${GeldBetrag::class}")


        // AUFGABE: Reaktiviere den Code ab hier.
        //
        // TIPP: Verschiebe den Anfangsmarkgiter des auskommmentierten Bereichs
        //       immer nur um jeweils eine Zeile nach unten, und versuche
        //       dann den Test wieder **grün** zu machen.
        //       Das ist wesentlich leichter, als wenn man sich
        //       um alle Probleme auf einmal kümmern muss.
        /*
        val b = GeldBetrag(47.11)

        assertThat(b.inCent).isEqualTo(4711)

        assertThat(b.mal(2).inCent).isEqualTo(9422)
        assertThat(b.plus(GeldBetrag(0.89)).inCent).isEqualTo(4800)

        assertThat(GeldBetrag(47.00).plus(GeldBetrag(0.11)))
                .isEqualTo(b)


        val (quotient, rest) = b.teile(4)

        assertThat(quotient).isEqualTo(GeldBetrag(11.77))
        assertThat(rest).isEqualTo(GeldBetrag(0.03))

        assertThat(quotient.mal(4).plus(rest))
                .isEqualTo(b)

        */
    }

}

