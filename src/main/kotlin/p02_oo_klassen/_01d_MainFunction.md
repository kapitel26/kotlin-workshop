## Ein paar erste aber wichtige Unterschiede zu Java

Trotz der Ähnlichkeiten, fallen bereits bei der `main`-Methde ein paar Unterschiede auf. Einige davon verweisen auch schon auf wichtige Merkmale der Sprache Kotlin:

 * `fun`
   - In Kotlin spricht man von *Funktionen* (statt Methoden)
   - Funktionen werden `fun` deklariert
   
 * kein `class`
   - Funktionen können alleine, außerhalb von Klassen stehen.

 *  `args: Array<String>`
   - Achtung: Andere *Wierumigkeit*
   - In Kotlin steht die Typangabe am Ende Deklatione

 * kein `public`
   - ist Default in Kotlin
   - darf weggelassen werden

 * kein `static`
   - Einen `static`-Teil, haben Klassen in Kotlin nicht
   
 * kein `;`
   - Anweisungen müssen nicht mit Semikolon beendet werden

 * `println` ohne `System.out` davor
   - Einige ganze Reihe nützlicher Funktionen sind bereits Standardmäsig implementiert
   - Dies erspart manche Import-Deklaration
   


