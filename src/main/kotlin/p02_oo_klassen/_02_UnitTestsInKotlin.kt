package p02_oo_klassen

import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * ## Wie man Unit-Tests in Kotlin implementiert
 *
 *  * Die üblichen Java-Test-Framworks, hier beispielsweise *JUnit 4*,
 * können direkt genutzt werden.
 *
 *  * Unit-Tests können, wie in Java, als Funktionen (aka Methoden)
 * in einer Klasse definiert werden.
 *
 * */
class _02_UnitTestsInKotlin {

    /**
     * Annotationen und Methoden aus Java-Libraries, können genutzt werden.
     *
     * @see Test
     * @see assertEquals
     *
     */
    @Test
    fun _01_Ein_einfacher_Test() {
        assertEquals("Soll gleich sein", 4, 2 + 2)
    }


    /**
     * ## Methodennamen in Kotlin
     *
     * Die Namen von Methoden
     *
     * * dürfen Leerzeichen und Sonderzeichen enthalten,
     * * jedoch keine Punkte und Anführungszeichen,
     * * der Name muss dann in Backticks eingeschlossen werden.
     *
     */
    private fun `funktionsnamen dürfen Leerzeichen enthalten!`() {

        println("Aus einer Funktion mit einem langen Namen.")
    }


    @Test
    fun _02_Namen_dürfen_Keerzeichen_enthalten() {

        `funktionsnamen dürfen Leerzeichen enthalten!`()
    }

    /**
     * Auf diese Weise kann man den Testfällen besser lesbare Namen geben.
     */
    @Test
    fun `_03 Korrekte Antwort auf eine außerordenlich wichtige Frage`() {

        assertEquals("the answer", 42, 6 * 7)
    }

    /**
     * ## Nutzung von Java-Libraries
     *
     *  * `import`-Statement fast wie in Java, jedoch ohne `static` beim Methodenimport:
     *
     *     ```
     *     import org.assertj.core.api.Assertions.assertThat
     *     ```
     *
     *  * Hier beispielsweise [AssertJ](http://joel-costigliola.github.io/assertj)
     *
     */
    @Test
    fun `_04 Assertions von AssertJ können genutzt werden`() {

        assertThat("Frodo").contains("do")

    }

    /**
     * ## Keyword-Konflikte
     *
     * Mit Backticks kann man auch Java-Methoden aufrufen,
     * deren Namen in Kotlin Schlüsselwörter sind, z. B. `as` oder `where`
     */
    @Test
    fun `_05 Aufrufen von Java-Funktionen, die in Kotlin Keyword sind`() {

        assertThat(6 * 7)
                .`as`("the answer")
                .isEqualTo(42)

    }

    /**
     * ## String Interpolations
     *
     * Außer Java unterstützten es wohl die meisten populären Programmiersprachen:
     * *String Interpolation*.
     *
     * * Mit `$` können die Werte von Variablen in Strings eingesetzt werden.
     * * Mit `${...}` können die Werte beliebiger Ausdrücke in Strings eingesetzt werden.
     */
    @Test
    fun `_06 String Interpolation`() {

        val q = "die Frage"

        assertEquals(
                "Die Antwort auf $q ist ${6 * 7}",
                "Die Antwort auf die Frage ist 42"
        )
    }


}

