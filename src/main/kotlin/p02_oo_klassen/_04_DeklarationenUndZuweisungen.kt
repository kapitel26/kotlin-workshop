package p02_oo_klassen


import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/**
 * ## *Values* und *Variables*
 *
 * Kotlin unterscheidet zwischen *Values* und *Variables*: `val` und `var`.
 *
 * * `val` entspricht im wesentlichen einer `final` Deklararion in Java,
 *    d.h. es darf nur ein einziges Mal ein Wert zugeordnet werden.
 * * `var`'s hingegen dürfen jederzeit neue Werte zugewiesen bekommen.
 *
 */
class _04_DeklarationenUndZuweisungen {


    /**
     * ## Einfache Variablen und Werte- Deklarationen
     *
     * * Es wird immer ein `val` (oder `var`) vorangestellt.
     *   - Ausnahme: Parameter von Funktionen (siehe unten).
     *
     * * Die Typangabe steht, mit `:` abgetrennt, hinter dem Namen.
     *
     **/
    @Test
    fun `01 Simple deklaration`() {


        val theAnswer1: Int
        theAnswer1 = 42


        val theAnswer2: String = "zweiundvierzig"


        assertThat(theAnswer1).isEqualTo(42)
        assertThat(theAnswer2).isEqualTo("zweiundvierzig")

    }

    /**
     * ## `val` vs. `var`
     *
     * Das Keyword `final` gibt es in Kotlin nicht,
     * statt dessen wird allen Deklarationen entweder `val`
     * oder `var` vorangestellt, um zu bestimmen, ob
     * Neuzuweisungen erlaubt sind.
     *
     */
    @Test
    fun `02 val vs var`() {


        val aValue: Int = 42
        // aValue = 4711        // keine erneute Zuweisung erlaubt


        var aVariable: Int = 11
        aVariable = 23


        assertThat(aValue).isEqualTo(42)
        assertThat(aVariable).isEqualTo(23)
    }


    /**
     * ## Funktionsparameter sind immer `val`
     *
     */
    @Test
    fun `03 bei Parametern ist val oder war nicht anzugeben, es gilt val`() {


        fun verdopple(n: Int): Int {

            // n = 4711     // Keine erneute Zuweisung erlaubt!

            return n + n
        }


        assertThat(verdopple(21)).isEqualTo(42)
    }


    /**
     * ## `val` heißt nicht Seiteneffekt-Frei!
     *
     * Ähnlich wie `final` in Java schützt `val` alleine nicht vor Seiteneffekten.
     *
     */
    @Test
    fun `04 Der Inhalt strukturierter Objekte kann auch bei Values nachträglich geändert werden`() {


        val aJavaArrayList: ArrayList<Int> = ArrayList()
        aJavaArrayList.add(42)


        assertThat(aJavaArrayList).contains(42)


        aJavaArrayList.clear()


        assertThat(aJavaArrayList).isEmpty()
    }

    /**
     * ## Type Inference
     *
     * Die Typeangaben kann (und sollte oft auch) weggelassen
     * werden wenn der Typ anhand des Zugewiesenen Wertes
     * ermittelt werden kann.
     *
     */
    @Test
    fun `05 Die Typangabe ist Optional, wenn der Typ anhand einer Zuweisung erkennbar ist`() {


        val theAnswer = 42


        assertThat(theAnswer).isEqualTo(42)
        assertThat(theAnswer is Int)
    }


}

