package p02_oo_klassen

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/**
 * ## Data Classes
 *
 * In Java ist das Entwurfsmuster *Data Transfer Object* verbreitet.
 * Dabei geht es um Objekte, die meist keine eigene Fachlogik besitzen und
 * primär Träger von Informationen.
 *
 * In Kotlin gibt es dafür syntaktische Unterstützung: `data class`.
 *
 * * Haben Felder (`val`/`var`) wie andere Klassen.
 *
 * * Haben zusätzlich automatische Implementierungen für
 *   - `equals(...)`
 *   - `hashCode(...)`
 *   - `copy(...)`
 *   - und mehr
 *
 * * Können oft in einer Zeile deklariert werden
 *
 * * Ersparen viel Boilerplate-Code
 *
 */
class _07_Data_Classes {

    @Test
    fun `01 Data Class`() {


        data class Rechteck(val breite: Int, val hoehe: Int)


        var r = Rechteck(3, 4)

        assertThat(r.toString())
                .isEqualTo("Rechteck(breite=3, hoehe=4)")

        assertThat(r)
                .isEqualTo(Rechteck(3, 4))
                .isNotEqualTo(Rechteck(11, 4))
                .isNotEqualTo(Rechteck(3, 11))
                .`as`("Gleichheit durch Feldvergleiche")

    }

    /***
     * ### Die `copy`-Methode bei Data Classes
     *
     * * Erzeugt eine Kopie, bei der einige Feldwert anders besetzt sind.
     *
     * * Besonders nützlich für den Umgang mit *Immutable Value Objects*.
     *
     */
    @Test
    fun `02 Kopieren von Datenobjekten`() {

        data class Name(val vorname: String, val nachname: String)

        val n1 = Name("Bjørn", "Stachmann")


        val n2 = n1.copy(vorname = "René")
        val n3 = n2.copy(nachname = "Preißel")


        assertThat(n1).isEqualTo(Name("Bjørn", "Stachmann"))
        assertThat(n2).isEqualTo(Name("René", "Stachmann"))
        assertThat(n3).isEqualTo(Name("René", "Preißel"))
    }

    /**
     * ### Data Classes als Rückgabewert von Funktionen
     *
     * *Data Classes* eignen sich als Rückgabewert für Funktionen,
     * die mehrere Ergebnisse liefern.
     * Per *Destructuring* können die Felder im Rückgabewert unterschiedlichen
     * Variablen zugewiesen werden.
     */
    @Test
    fun `03 Data Classes and Destructuring`() {

        data class ShellOutput(val out: String, val err: String, val exitCode: Int)

        fun callExternalScript(): ShellOutput {
            // call shell script and capture output
            return ShellOutput("fake-output", "fake-error-message", 42)
        }


        val (stdout, stderr, exit) = callExternalScript()


        assertThat(stdout).isEqualTo("fake-output")
        assertThat(stderr).isEqualTo("fake-error-message")
        assertThat(exit).isEqualTo(42)
    }
}