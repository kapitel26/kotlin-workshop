# Package p02_oo_klassen

## Prozeduren, Funktionen, Klassen und Objekte

In diesem Kapitel zeigen wir (gerade eben soviel)
Syntax von Kotlin, dass Du damit einfache Java-Klassen und JUnit-Tests nach Kotlin portieren kannst.

Im ersten Schritt geht es um eine strukturgleiche, also 1:1, Portierung. In den folgenden Kapiteln lernst Du dann Features von Kotlin kennen, die es ermöglichen, Dinge anders zu implementieren, als man es in Java tun würde

Du lernst hier

 * eine `main`-Methode zu erstellen,
 * *Funktionen* (in Java *Methoden* genannt)
    - zu implementieren
    - und aufzurufen,
 * *Unit-Tests* zu schreiben, und
 * *Klassen*
   - anzulegen und
   - deren *Konstruktoren* aufzurufen und
   - und *Objekte* zu verwenden.
