package p02_oo_klassen

/**
 * ## Übung: Portierung von Java-Klassen
 *
 * Im Package [p02_oo_klassen._09b.uebung] befinden sich drei Java-Klassen.
 * Diese sollen mit Hilfe der *Kotlin-Konvertierung* von *IntelliJ*
 * portiert werden. Das Ergebnis wird funktionieren, aber
 * es wird kein *idiomatischer Kotlin-Code* werden.
 * Ziel dieser Übung ist es, den portieren Code Schritt für Schritt
 * zu verbessern, so dass kompakterer Kotlin-Code ensteht.
 *
 *
 * 1. Markiere das Package [p02_oo_klassen._09b.uebung] und
 *    öffne das Kontextmenü. Wähle "Convert Java File to Kotlin File"!
 *
 * 1. Starte die Tests in [p02_oo_klassen._09b.uebung.BruchTest].
 *
 *    - die Test sollten Grün sein
 *
 *
 *    1. Wähle eine Maßnahme. Ein paar Vorschläge:
 *
 *      - Ziehe eine Hilfsfunktion aus dem `object`-Konstrukt auf den Top-Level
 *      - Ziehe eine Hilfsfunktion in die Datei um, wo sie verwendet wird
 *      - Deklariere Felder bereits im primären Konstruktor
 *      - Weise Funktionen einen Ausdruck zu, anstatt einen Block zu verwenden.
 *      - Konvertiere eine Klasse zu einer *Data Class*
 *        um `equals()` und `hashCode()` loszuwerden
 *      - Extrahiere eine factory-Metode mit der Validierungs- und Kürzungs  logik,
 *        damit die Implementierun verborben und
 *        der primäre Konstruktor als `private` geschützt werden kann.
 *      - Ersetze `if-then`-Kette durch `when`
 *      - Gebe den Tests sprechende Namen mit ganzen Sätzen
 *
 *    1. Setze die Maßnahme um
 *    1. Mache die Test **grün**
 *    1. Nächste Maßnahme. Bis der Code schön kompakt ist.
 *
 * [Siehe auch: Idiome in Kotlin](https://kotlinlang.org/docs/reference/idioms.html)
 */
class _09a_Uebung_AutomatischKonvertieren


// HINWEIS: Diese Übung wird im Package p02_oo_klassen._09b.uebung bearbeitet.

