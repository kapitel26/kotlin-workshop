package p02_oo_klassen

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import kotlin.math.PI

/**
 * ## Properties
 *
 * Kaum etwas prägt die Optik von Java-Source-Code so sehr wie *Getter*
 * und *Setter*. Auf der einen Seite kapseln sie Implementerierungdetails
 * über die Speicherung von Feldern, so dass man beispielsweise nachträglich
 * ein Caching oder Lazy-Initialize einbauen kann, ohne bestehende
 * APIs zu brechen. Auf der anderen Seite blähen sie selbst kleine Hilfsklassen
 * zu beeindruckenden Zeilenmonstern auf.
 *
 * Viele Sprachen lösen das Dilemma zwischen Kapselung und Code-Menge
 * durch syntaktische Unterstützung für *Properties*. So auch Kotlin.
 *
 */

class _06_Properties {

    @Test
    fun `02 Propertyzugriff sieht aus, wie Zugriff auf öffentlich Felder in Java`() {

        class Rechteck(var breite: Int, var hoehe: Int)

        var r = Rechteck(3, 4)


        r.breite = r.breite + 1
        r.hoehe = r.hoehe + 1


        assertThat(r.breite).isEqualTo(4)
        assertThat(r.hoehe).isEqualTo(5)
    }

    /**
     * ### Getter implementieren
     *
     * * An die Deklaration eines Feldes kann mit `get() ...`
     *   ein Getter angehängt werden.
     *
     * * Der Aufruf sieht aus, wie ein normaler Feldzugriff.
     *
     */
    @Test
    fun `02 Berechneter Getter`() {

        class Rechteck(val breite: Int, val hoehe: Int) {


            val flaeche: Int
                get() = breite * hoehe


        }

        var r = Rechteck(3, 4)

        assertThat(r.flaeche).isEqualTo(12)
    }

    /**
     * ### Setter
     *
     * * Bei `var`-Feldern kann auch ein Setter definiert werden.
     *
     * * Mit `set(v) { ... }` hinter der Feld-Deklaration.
     *
     */
    @Test
    fun `03 Codierte Getter und Setter`() {

        class Winkel(var radiant: Double) {


            var grad: Double
                get() = radiant * 180.0 / PI
                set(gradValue) {
                    radiant = gradValue / 180.0 * PI
                }


        }

        var w = Winkel(PI)


        w.grad = 45.0


        assertThat(w.radiant).isEqualTo(PI / 4.0)
    }

}