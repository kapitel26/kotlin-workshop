# Workshop Agenda
* 9:00 Uhr - 17:00 Uhr
* Einstieg: 30 min
    * Installation und Intro
* 5 Blöcke ca 60 min (45min Präsentation und Code / 15min Übungen)
    * Grundlagen / Objektorientierung / Klassen
    * Typsystem und Funktionen
    * Erweiterte Features und Integration in Spring
    * DSLs erstellen und Beispiele anhand realer Frameworks
    * Koroutinen
* Ausblick: 30 min
* Zeit für Fragen und Experimente

## Zeiten

| Wann                 | Was                                     |
|----------------------|-----------------------------------------|
|  09:00 - 09:30       |**Intro**                                |
|                      |**Part 1: Von Java nach Kotlin**         |
|  09:30 - 10:30       |Klassen und Objekte                      |
|      Pause           |                                         |
|  11:00 - 12:00       |Typsystem und Funktionale Programmierung |
|                      |**Part 2: Mehr als Java**                |
|  12:00 - 12:30       |Erweiterte Features                      |
|     Mittag           |                                         |
|  13:30 - 14:00       |Erweiterte Features                      |
|  14:00 - 15:00       |DSLs                                     |
|      Pause           |                                         |
|  15:30 - 16:30       |Koroutinen                               |
|  16:30 - 17:00       |**Ausblick und Fragen**                  |


# Software Voraussetzungen
 
* [Java 8 - 11](http://jdk.java.net/11/)
* [IntellijIDEA Community Edition](https://www.jetbrains.com/idea/download/)
* [Aktuelles Kotlin Plugin >= 1.2.71](https://plugins.jetbrains.com/plugin/6954-kotlin)
* [Markdown Plugin](https://plugins.jetbrains.com/plugin/7896-markdown-navigator)


# Beispiele und Übungen für den Workshop

* Software für Plattform vom USB-Stick kopieren

## Installation
* Für Windows: Installieren Sie 7-Zip-Portable
    * Nutzen Sie 7-Zip für die nachfolgenden Entpack-Schritte
* Entpacken Sie das OpenJdk-Archive, wenn kein passendes JRE (8 - 11) vorhanden ist
* Entpacken Sie das gradle-cache.zip
* Installieren Sie die IntellijIDEA Community Edition
    * Kein JRE Download anstoßen
* Starten Sie Intellij und importieren Sie das Workshop-Projekt
    * Wählen Sie Gradle als "Externes Model" aus
    * Konfigurieren Sie das OpenJDK
    * Setzen Sie Gradle auf "Offline work"
    * Setzen Sie den "Service Directory Path" auf das ausgepackte Gradle-Cache-Verzeichnis

    ![](_gradle_offline_marked.png)

* Installieren Sie das Kotlin-Plugin von der kopierten Datei: kotlin-plugin-1.2.71-release-IJ2018.2-1.zip
    * File/Settings/Plugins/"Install plugin from disk ..."
* Installieren Sie das Markdown-Plugin von der kopierten Datei: idea-multimarkdown.2.6.0.zip
    * File/Settings/Plugins/"Install plugin from disk ..."
