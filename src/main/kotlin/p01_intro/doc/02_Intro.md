# Kotlin Entstehung
* Kotlin ist auch eine Insel - vor Sankt Petersburg 
* JetBrains stellt die Hauptentwickler der Sprache 
* __2011 Erste veröffentlichte Version__ 
* 2012 Unter Apache-2-Lizenz gestellt
* 2012 KotlinJS wird vorgestellt
* __2016 Version 1.0 veröffentlicht__
* 2017 Kotlin Native wird vorgestellt
* __2017 Kotlin wird offizielle Sprache für Android__


# Warum Entwickler Kotlin lieben?
* [Umfrage März 2018](https://pusher.com/state-of-kotlin?section=dev-favorite)
    * 2744 Beteiligte
    
    ![](_kotlin_growth.png)
    
    ![](_kotlin_einsatz.png)
    
    ![](_kotlin_favorites.png )

# Weitere Umfrage  
* [Jetbrains Anfang 2018](https://www.jetbrains.com/research/devecosystem-2018/kotlin/)
    
    ![](_survey_type.png)

    ![](_survey_version.png)


# Design Prinzipien Kotlin
* Interoparabilität mit Java 
* Einfachheit und Lesbarkeit
* Gute Unterstützung von DSLs
* Schneller Compiler 
* Pragramatische Lösungen vor akademischer Vollständigkeit
* Unterstützung von objektorientierter, prozeduraler und funktionaler Programmierung

