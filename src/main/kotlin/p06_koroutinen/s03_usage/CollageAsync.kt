@file:Suppress("PackageDirectoryMismatch")

package p06_koroutinen.s03_usage.async

import com.jayway.jsonpath.JsonPath
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.awaitAll
import kotlinx.coroutines.experimental.coroutineScope
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.selects.select
import p06_koroutinen.helper.JerseyClient
import p06_koroutinen.helper.Timer
import p06_koroutinen.helper.combineImages
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.io.InputStream
import javax.imageio.ImageIO
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType
import kotlin.coroutines.experimental.suspendCoroutine


fun main(args: Array<String>): Unit = runBlocking {
    JerseyClient.use {
        val collageTurtle = createCollageAsyncAwait("turtle", 20)
        ImageIO.write(collageTurtle, "png", FileOutputStream("turtle.png"))
    }
    Unit
}


private suspend fun createCollageAsyncAwait(
    query: String, count: Int
): BufferedImage = coroutineScope {
    val urls = requestImageUrls(query, count)
    val deferredImages: List<Deferred<BufferedImage>> = urls.map {
        async {
            requestImageData(it)
        }
    }

    val images: List<BufferedImage> = deferredImages.awaitAll()
//    for (deferredImage in deferredImages) {
//        val image = deferredImage.await()
//    }

    val newImage = combineImages(images)
    newImage
}

suspend fun loadFastestImage(query: String, count: Int): BufferedImage = coroutineScope {
    val urls = requestImageUrls(query, count)
    val deferredImages = urls.map {
        async { requestImageData(it) }
    }
    val image: BufferedImage = select {
        for (deferredImage in deferredImages) {
            deferredImage.onAwait { image ->
                image
            }
        }
    }
    deferredImages.forEach { it.cancel() }
    image
}


suspend fun loadFastImages(urls: List<String>, timeoutMs: Long): List<BufferedImage> = coroutineScope {
    val timer = Timer(timeoutMs)
    val result = mutableListOf<BufferedImage>()
    val deferredImages = urls.map {
        async { requestImageData(it) }
    }

    val imagesToRetrieve = deferredImages.toMutableSet()
    while (timer.isRunning() && imagesToRetrieve.isNotEmpty()) {
        select<Unit> {
            for (deferredImage in imagesToRetrieve) {
                deferredImage.onAwait { image ->
                    imagesToRetrieve -= deferredImage
                    result += image
                }
            }

            onTimeout(timer.timeToGo()) { }
        }

    }
    imagesToRetrieve.forEach { it.cancel() }
    result
}

//<editor-fold defaultstate="collapsed" desc="Details">
private suspend fun requestImageUrls(
    query: String,
    count: Int = 20
) = suspendCoroutine<List<String>> { continuation ->
    JerseyClient.pixabay("q=$query&per_page=$count").request().async()
        .get(object : InvocationCallback<String> {
            override fun completed(response: String) {
                val urls = JsonPath.read<List<String>>(response, "$..previewURL")
                continuation.resume(urls)
            }

            override fun failed(throwable: Throwable) {
                continuation.resumeWithException(throwable)
            }
        })
}

private suspend fun requestImageData(imageUrl: String) = suspendCoroutine<BufferedImage> { cont ->
    JerseyClient.url(imageUrl)
        .request(MediaType.APPLICATION_OCTET_STREAM)
        .async()
        .get(object : InvocationCallback<InputStream> {
            override fun completed(response: InputStream) {
                val image = ImageIO.read(response)
                cont.resume(image)
            }

            override fun failed(throwable: Throwable) {
                cont.resumeWithException(throwable)
            }
        })
}
//</editor-fold>


