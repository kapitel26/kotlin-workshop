@file:Suppress("PackageDirectoryMismatch")
package p06_koroutinen.s01_intro.collagecoroutine

import com.jayway.jsonpath.JsonPath
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.yield
import p06_koroutinen.helper.JerseyClient
import p06_koroutinen.helper.combineImages
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.io.InputStream
import javax.imageio.ImageIO
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType
import kotlin.coroutines.experimental.suspendCoroutine

fun main(args: Array<String>): Unit = runBlocking {
    JerseyClient.use {
        val collageTurtle = createCollage("turtle", 20)
        ImageIO.write(collageTurtle, "png", FileOutputStream("turtle.png"))
        val collageDogs = createCollage("dogs", 20)
        ImageIO.write(collageDogs, "png", FileOutputStream("dogs.png"))
        val collageLions = createCollage("lion", 20)
        ImageIO.write(collageLions, "png", FileOutputStream("lions.png"))
    }
    Unit
}


suspend fun createCollage(
    query: String, count: Int
): BufferedImage {
    val urls = requestImageUrls(query, count)
    val images = urls.map { requestImageData(it) }
    val newImage = combineImages(images)
    return newImage
}

//<editor-fold defaultstate="collapsed" desc="Details">
private suspend fun requestImageUrls(
    query: String,
    count: Int = 20
) = suspendCoroutine<List<String>> { continuation ->
    JerseyClient.pixabay("q=$query&per_page=$count").request().async()
        .get(object : InvocationCallback<String> {
            override fun completed(response: String) {
                val urls = JsonPath.read<List<String>>(response, "$..previewURL")
                continuation.resume(urls)
            }

            override fun failed(throwable: Throwable) {
                continuation.resumeWithException(throwable)
            }
        })
}

private suspend fun requestImageData(imageUrl: String) = suspendCoroutine<BufferedImage> { cont ->
    JerseyClient.url(imageUrl)
        .request(MediaType.APPLICATION_OCTET_STREAM)
        .async()
        .get(object : InvocationCallback<InputStream> {
            override fun completed(response: InputStream) {
                val image = ImageIO.read(response)
                cont.resume(image)
            }

            override fun failed(throwable: Throwable) {
                cont.resumeWithException(throwable)
            }
        })
}
//</editor-fold>


