@file:Suppress("PackageDirectoryMismatch")
package p06_koroutinen.s01_intro.oneimagethread

import com.jayway.jsonpath.JsonPath
import p06_koroutinen.helper.JerseyClient
import java.awt.image.BufferedImage
import java.io.InputStream
import javax.imageio.ImageIO
import javax.ws.rs.core.MediaType

fun main(args: Array<String>) = JerseyClient.use {
    val image = loadOneImage("dogs")
    println("${image.width}x${image.height}")
}

fun loadOneImage(query: String): BufferedImage {
    val url = requestImageUrl(query)
    val image = requestImageData(url)
    return image
}

//<editor-fold defaultstate="collapsed" desc="Details">


private fun requestImageUrl(query: String): String {
    val json = JerseyClient.pixabay("q=$query&per_page=200")
        .request()
        .get(String::class.java)
    return JsonPath.read<List<String>>(json, "$..previewURL").shuffled().firstOrNull()
            ?: throw IllegalStateException("No image found")
}

private fun requestImageData(imageUrl: String): BufferedImage {
    val inputStream = JerseyClient.url(imageUrl)
        .request(MediaType.APPLICATION_OCTET_STREAM)
        .get(InputStream::class.java)
    return ImageIO.read(inputStream)
}


//</editor-fold>