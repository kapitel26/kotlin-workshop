# Koroutinen

 * Melvin Conway 1963
 * Kooperative Übergabe des Kontrollflusses
 * Koroutinen sind *sequentiell* per Default

---

# Funktionen / Routinen
![](_routine.png)

---

# Koroutinen
![](_coroutine.png)

---

# Stackless vs Stackfull Koroutinen

* Stackless: Suspendierungen sind nur __direkt__ in Koroutinen möglich
* Stackfull: Suspendierungen sind __überall__ möglich
* Kotlin implementiert __stackless__ Koroutinen

---

# Continuations

```kotlin
suspend fun createCollage(query: String, count: Int): BufferedImage {
    val urls = requestImageUrls(query, count) //Label 0
    val images = mutableListOf<BufferedImage>() //Label 1
    for (index in 0 until urls.size) {
        val image = requestImageData(urls[index])
        images += image //Label 2
    }
    val newImage = combineImages(images)
    return newImage
}
```

![](_continuation.png)

---

# Kotlin Compiler

Ausgangscode: 

```kotlin
suspend fun createCollage(
    query: String, count: Int
): BufferedImage {
    ...
}
```

Compiler erzeugt daraus:

```kotlin
fun createCollage(
    query: String, count: Int,
    parentContinuation: Continuation<BufferedImage>
): Any // BufferedImage | COROUTINE_SUSPENDED {
    val cont = CoroutineImpl(parentContinuation) //Implements Continuation
    ...
}
```

![](_continuation-full.png)

---

##### Continuations-Stack

![](_continuation-list.png)

---

##### Einstieg und Absprung?

![](_coroutine-builder-lib.png)

---
# Builder - Einstieg in Koroutinen

```kotlin
//Startet die Koroutine und "blockiert" den aktuellen Thread
val collage = runBlocking {
    createCollage("dogs", 20)
}

//Startet die Koroutine und setzt den aktuellen Thread fort
val job = launch {
    val collage = createCollage("dogs", 20)
    ImageIO.write(collage, "png", FileOutputStream("dogs.png"))
}

//Stoppt die Koroutine
job.cancel()
```

---

# CoroutineContext

```kotlin
//Den Fork-Join-Pool für die Koroutine nutzen
val collage = runBlocking(Dispatchers.Default) {
    createCollage("dogs", 20)
}

//Einen eigenen Thread-Pool für die Koroutine nutzen
val fixedThreadPoolContext = newFixedThreadPoolContext(1, "collage")
val job = launch(fixedThreadPoolContext) {
    val collage = createCollage("dogs", 20)

    // Wechsel in den UI-Thread und zurück
    withContext(Dispatchers.UI) {
        ImageIO.write(collage, "png", FileOutputStream("dogs.png"))
    }
}
```
---

# Structured Concurrency - CoroutineScope

* Neues Konzept seit Version 0.26.0 ([Theorie](https://vorpus.org/blog/notes-on-structured-concurrency-or-go-statement-considered-harmful/), [Umsetzung in Kotlin](https://medium.com/@elizarov/structured-concurrency-722d765aa952))

* Die Idee dabei ist, dass Koroutinen die weitere Koroutinen starten (Kinder), selber erst beendet werden, wenn alle Kinder
  fertig sind. Falls ein Kind eine Exception nicht behandelt und deswegen beendet wird, 
  werden dessen eigene Kinder, alle Geschwister und die Eltern-Koroutine ebenso beendet.
  
* Das Ziel dabei ist, ähnlich wie bei strukturierter Programmierung, Klammern (Scopes) zu bilden,  
  die garantieren, dass keine Koroutine unabsichtlich noch läuft.
  
* Deswegen sind die `Builder` zum starten von neuen Koroutinen als globale Funktionen entfernt worden und müssen
  in der neuen Version immer an einem Scope aufgerufen werden. 
  Ausnahme bildet der `GlobalScope` derren Kinder keine Abhängigkeit zu einer Eltern-Koroutine haben.    
  

siehe `02_concepts/StructuredConcurrency`

---
# Integration mit asynchronen Libraries

```kotlin
suspendCoroutine<List<String>> { continuation ->
    JerseyClient.pixabay("q=$query&per_page=$count").request().async()
        .get(object : InvocationCallback<String> {
            override fun completed(response: String) {
                val urls = JsonPath.read<List<String>>(response, "$..previewURL")
                continuation.resume(urls)
            }

            override fun failed(throwable: Throwable) {
                continuation.resumeWithException(throwable)
            }
        })
}
```

siehe: `s01_intro/OneImageCoroutinen::requestImageUrl`

