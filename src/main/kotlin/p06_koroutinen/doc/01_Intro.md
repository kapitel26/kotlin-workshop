# Warum

![](_utilization.png)    

---

# Blocking vs Non-Blocking

![](_blocking.png)    

---

# Beispiel: Collage

![](_turtle.png)    

 ```kotlin
 val collage = createCollage("turtle", 20)
 ```
 
# Arbeiten ohne Koroutinen

## Einfaches Beispiel 
 * Threads:             `s01_intro/OneImageThread::loadOneImage`
 * Callbacks:           `s01_intro/OneImageCallback::loadOneImage`
 * Future / Promises:   `s01_intro/OneImageFuture::loadOneImage`

## Komplexeres Beispiel
 * Threads:             `s01_intro/CollageThread::createCollage`
 * Callbacks:           `s01_intro/CollageCallback::createCollage`
 * Future / Promises:   `s01_intro/CollageFuture::createCollage`
 
# Arbeiten mit Koroutinen
 * Ein Bild laden:      `s01_intro/OneImageCoroutinen::loadOneImage`
 * Collage erzeugen:    `s01_intro/CollageCoroutinen::createCollage`
 