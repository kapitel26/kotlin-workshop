# Asynchrone Muster / Konzepte in Kotlin

* Sequential by default
* Asynchronous explicitly
* Libraries not language

---

# Sequential by default

```kotlin
suspend fun createCollage(query: String, count: Int): BufferedImage {
    val urls = requestImageUrls(query, count)
    val images = urls.map { requestImageData(it) }
    val newImage = combineImages(images)
    return newImage
}
```

---

# Async / Await explizit

```kotlin
suspend fun createCollageAsyncAwait(
    query: String, count: Int
): BufferedImage = coroutineScope {
    val urls = requestImageUrls(query, count)
    val deferredImages: List<Deferred<BufferedImage>> = urls.map {
        async {
            requestImageData(it)
        }
    }

    val images: List<BufferedImage> = deferredImages.awaitAll()

    val newImage = combineImages(images)
    newImage
}
```
siehe `s03_usage/CollageAsync::createCollageAsyncAwait

---

# Auf das erste Ereignis warten - Select

```kotlin
suspend fun loadFastestImage(query: String, count: Int): BufferedImage = coroutineScope {
    val urls = requestImageUrls(query, count)
    val deferredImages = urls.map {
        async { requestImageData(it) }
    }
    val image: BufferedImage = select {
        for (deferredImage in deferredImages) {
            deferredImage.onAwait { image ->
                image
            }
        }
    }
    deferredImages.forEach { it.cancel() }
    image
}
```
siehe `s03_usage/CollageAsync::loadFastestImage
siehe `s03_usage/CollageAsync::loadFastestImages

---

# Channels
## Nachrichten senden

```kotlin
suspend fun retrieveImages(query: String, channel: SendChannel<BufferedImage>) {
    while (true) {
        val url = requestImageUrl(query)
        val image = requestImageData(url)
        channel.send(image)
        delay(2, TimeUnit.SECONDS)
    }
}
```
siehe `s03_usage/CollageChannel

---

## Nachrichten empfangen

```kotlin
suspend fun createCollage(channel: ReceiveChannel<BufferedImage>, count: Int) {
    var imageId = 0
    while (true) {
        val images = (1..count).map {
            channel.receive()
        }
        val collage = combineImages(images)
        ImageIO.write(collage, "png", FileOutputStream("image-${imageId++}.png"));
    }
}
```

siehe `s03_usage/CollageChannel

---

## Channels erzeugen

```kotlin
val channel = Channel<BufferedImage>()
launch(Unconfined) {
    retrieveImages("dogs", channel)
}

launch(Unconfined) {
    retrieveImages("cats", channel)
}

launch(Unconfined) {
    createCollage(channel, 4)
}
```

siehe `s03_usage/CollageChannel

---

# Unconfined und Rendezvous

```
"jersey-client-async-executor-2@3321" prio=5 tid=0x13 nid=NA runnable
  java.lang.Thread.State: RUNNABLE
	  at ...CSPChannelKt.createCollage(CSPChannel.kt:47)
	  at ...CSPChannelKt$createCollage$1.doResume(CSPChannel.kt:-1)
	  at ...CoroutineImpl.resume(CoroutineImpl.kt:54)
	  at ...ResumeModeKt.resumeMode(Dispatched.kt:87)
	  at ...DispatchedKt.dispatch(Dispatched.kt:193)
	  at ...AbstractContinuation.afterCompletion(AbstractContinuation.kt:86)
	  at ...JobSupport.completeUpdateState$kotlinx_coroutines_core(Job.kt:719)
	  at ...CancellableContinuationImpl.completeResume(CancellableContinuation.kt:264)
	  at ...AbstractChannel$ReceiveElement.completeResumeReceive(AbstractChannel.kt:817)
	  at ...AbstractSendChannel.offerInternal(AbstractChannel.kt:64)
	  at ...AbstractSendChannel.offer(AbstractChannel.kt:186)
	  at ...AbstractSendChannel.send(AbstractChannel.kt:180)
	  at ...CSPChannelKt.retrieveImages(CSPChannel.kt:59)
```

---

# Producer / Consumer

## Producer
```kotlin
suspend fun retrieveImages(
    query: String
): ReceiveChannel<BufferedImage> = currentScope {
    produce {
        while (isActive) {
            try {
                val url = requestImageUrl(query)
                val image = requestImageData(url)
                send(image)
                delay(2, TimeUnit.SECONDS)
            } catch (exc: Exception) {
                delay(1, TimeUnit.SECONDS)
            }
        }
    }
}

val dogsChannel = retrieveImages("dogs")
val catsChannel = retrieveImages("cats")

```
siehe `s03_usage/CollageProducerConsumer

---

## Consumer

```kotlin
suspend fun createCollage(
    count: Int,
    vararg channels: ReceiveChannel<BufferedImage>
): BufferedImage {
    val images = (1..count).map {
        selectUnbiased<BufferedImage> {
            channels.forEach { channel ->
                channel.onReceive { it }
            }
        }
    }
    return combineImages(images)
}

createCollage(4, catsChannel, dogsChannel)
```

siehe `s03_usage/CollageProducerConsumer

---

# Sonstiges
 * Actor
 * Zusammenarbeit mit Reaktiven Libraries (Webflux, RxJava, etc)
 
 
# Zusammenfassung

*__```suspend```__ konvertiert Funktionen zu Koroutinen<
* Sequential by Default / Asynchronous explicitly
* Asynchronen Kommunikationsmustern als Library
* Einfache Integration in vorhandene asynchrone APIs
* Tooling / Debugging muss noch verbessert werden
* Stackless: Suspendierungen nur in Koroutinen möglich [(Red/Blue Code Problem)](http://journal.stuffwithstuff.com/2015/02/01/what-color-is-your-function/)</li>

---

# Red-Blue-Problem

## Alles Routinen  
  ![](_red-blue-1.png)

---

# C() wird zur Koroutine
  ![](_red-blue-2.png)

---

## Ausblick

 * Stackfull-Koroutinen durch [__Quasar__](https://github.com/puniverse/quasar)

 * Oder durch Project [__Loom__](http://cr.openjdk.java.net/~rpressler/loom/Loom-Proposal.html) ([Idee](https://www.youtube.com/watch?v=fpyub8fbrVE))
  