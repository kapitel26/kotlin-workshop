package p06_koroutinen.w01_coroutine

import java.lang.management.ManagementFactory
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.concurrent.thread

/*
  Erzeugen Sie eine Kopie dieser Datei und
  konvertieren Sie den folgenden Code von Threads zu Koroutinen
  Tipps:
  * Die gesamte main-Funktion muss mit runBlocking umschlossen werden
  * Aus thread wird launch
  * Aus submit wird async
  * Aus sleep wird delay
  * Aus Future.get wird Deferred.await
  Führen Sie die neue Main-Funktion aus und erklären Sie den Unterschied zur Thread-Variante
  Wieviele Threads werden neu erstellt?
 */
fun main(args: Array<String>) {
    val threadMX= ManagementFactory.getThreadMXBean()
    thread {
        while (true) {
            Thread.sleep(50)
            println("Threads: ${threadMX.threadCount}")
        }
    }

    val executor = Executors.newCachedThreadPool()
    val helloList = (1..100).map { index ->
        executor.submit(Callable {
            if (index == 42) {
                throw IllegalStateException()
            }
            Thread.sleep(100L + index)
            println("Hello from $index")
            index
        })
    }

    val result = helloList.map { it.get() }.joinToString()
    println(result)
}