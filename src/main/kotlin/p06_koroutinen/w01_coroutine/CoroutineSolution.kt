package p06_koroutinen.w01_coroutine

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.lang.management.ManagementFactory

fun main(args: Array<String>) = runBlocking {
    val threadMX= ManagementFactory.getThreadMXBean()
    launch {
        while (true) {
            delay(50)
            println("Threads: ${threadMX.threadCount}")
        }
    }

    val helloList = (1..100).map { index ->
        async {
            if (index == 42) {
                throw IllegalStateException()
            }
            delay(100 + index)
            println("Hello from $index")
            index
        }
    }

    val result = helloList.map { it.await() }.joinToString()
    println(result)

}