package p06_koroutinen.s02_concepts

import kotlinx.coroutines.experimental.cancelChildren
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.util.concurrent.TimeUnit

fun main(args: Array<String>): Unit = runBlocking { // Erster Scope
    val job = launch { //Zweiter Scope

        for (i in 1..100) {
            launch { // Dritter Scope
                println("Start $i")
                try {
                    delay(1, TimeUnit.HOURS)
                } catch (e: Exception) {
                    println("${e::class} ${e.message}")
                } finally {
                    println("End $i")
                }
            }
        }
    }

    delay(2, TimeUnit.SECONDS)

    //Kinder des zweiten Scopes beenden
    job.cancelChildren()
}