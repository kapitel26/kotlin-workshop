package p06_koroutinen.helper

import org.glassfish.jersey.client.ClientConfig
import p06_koroutinen.helper.Constants.PIXABAY_URL
import java.io.Closeable
import javax.ws.rs.client.ClientBuilder


object JerseyClient : Closeable {
    private val client = ClientBuilder.newClient(ClientConfig())

    override fun close() {
        client.close()
    }

    fun pixabay(params: String) = client.target("$PIXABAY_URL&$params")
    fun url(url: String) = client.target(url)
}