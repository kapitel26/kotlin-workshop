import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension
import org.springframework.boot.gradle.dsl.SpringBootExtension

val kotlinCoroutineVersion by extra("0.26.0")
val ktorVersion: String by extra { "0.9.5" }
val kodeinVersion: String by extra { "5.2.0" }


plugins {
    val kotlinVersion = "1.2.71"

    id("org.jetbrains.kotlin.jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.allopen") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.jpa") version kotlinVersion
    id("org.springframework.boot") version "2.0.5.RELEASE"
    id("org.jetbrains.dokka") version "0.9.17"
}

apply {
    plugin("io.spring.dependency-management")
    plugin("org.jetbrains.dokka")
}


group = "de.e2"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
    maven("https://dl.bintray.com/kotlin/ktor")
    maven("https://dl.bintray.com/kotlin/kotlinx.html")
}

springBoot {
    mainClassName = "p03_extensions_reified.s0x_spring.springboot.dsl.HelloWorldControllerDSLKt"
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-runtime")
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    compile("org.jetbrains.kotlin:kotlin-reflect")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutineVersion")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:$kotlinCoroutineVersion")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:$kotlinCoroutineVersion")
    compile("org.springframework.boot:spring-boot-starter-webflux")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.6")

    compile("org.glassfish.jersey.core:jersey-client:2.26")
    compile("org.glassfish.jersey.inject:jersey-hk2:2.26")
    compile("org.glassfish.jersey.media:jersey-media-json-jackson:2.26")
    compile("com.jayway.jsonpath:json-path:2.3.0")
    runtime("javax.activation:activation:1.1.1")

    compile("io.github.microutils:kotlin-logging:1.4.4")

    compile("io.ktor:ktor-server-core:$ktorVersion")
    compile("io.ktor:ktor-server-netty:$ktorVersion")
    compile("io.ktor:ktor-locations:$ktorVersion")
    compile("io.ktor:ktor-html-builder:$ktorVersion")
    compile("io.ktor:ktor-gson:$ktorVersion")

    compile("org.kodein.di:kodein-di-generic-jvm:$kodeinVersion")
    compile("org.kodein.di:kodein-di-conf-jvm:$kodeinVersion")

    compile("com.google.code.gson:gson:2.8.5")
    compile("com.xenomachina:kotlin-argparser:2.0.7")
    compile("com.h2database:h2:1.4.196")

    implementation("junit:junit:4.4")
    implementation("org.assertj:assertj-core:3.11.1")

    compile("io.vertx:vertx-core:3.5.4")
    compile("io.vertx:vertx-lang-kotlin-coroutines:3.5.4")


}

kotlin {
    experimental.coroutines = Coroutines.ENABLE
}

tasks {
    val dokka by getting(DokkaTask::class) {
        includes =
                listOf("README.md",
                        "src/main/kotlin/p02_oo_klassen/_00_package.md"
                )
        outputFormat = "html"
    }

    task("browseDocs") {
        dependsOn(dokka)
        doLast {
            logger.info("echo firefox $buildDir/dokka/$name")
            Runtime.getRuntime().exec("firefox ${dokka.outputDirectory}/${project.name}/index.html")
        }
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict")
}

tasks {
    val hello by creating {
        doLast {
            println("Hello from Creating")
        }
    }

}