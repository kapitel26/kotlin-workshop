# Module kotlin

 > von René Preißel und Bjørn Stachmann

Kotlin nimmt Fahrt auf:
Immer mehr Projekte und Entwickler entscheiden sich für die elegante Sprache,
die eine kompakte und lesbare Syntax bietet,
ohne Abstriche in Typsicherheit und Java-Interoperabilität zu machen.

Dieser Workshop zeigt Euch wie man in Kotlin loglegt und was Kotlin kann.

[**Hier klicken und los geht's ...**](src/main/kotlin/p01_intro/doc/01_Installation.md)

Texte, Abbildungen, Sourcen, Tests und sonstiges Material findet ihr unter:


    src/main/kotlin
    
Dort ist alles durchnummeriert, so dass es der Reihenfolge im Workshop entspricht.

    p01_intro
    p02_oo_klassen
    p03_datentypen_und_funktionale_programmierung
    
      ...
    
    
## Lizenz

Diese Unterlagen zum Workshop sind unter *Creative Commons* [**Attribution-NonCommercial-NoDerivatives 4.0 International**](LICENSE) ([online](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.de)) lizenziert. 

